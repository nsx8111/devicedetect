//
//  ScreenViewController.h
//  deviceDetect
//
//  Created by 楊智偉 on 2019/1/30.
//  Copyright © 2019 楊智偉. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ScreenViewController : UIViewController
@property (nonatomic, copy) void (^finished)(BOOL success);
@end

NS_ASSUME_NONNULL_END
