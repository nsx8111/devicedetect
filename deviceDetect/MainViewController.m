//
//  MainViewController.m
//  deviceDetect
//
//  Created by 楊智偉 on 2019/2/24.
//  Copyright © 2019 楊智偉. All rights reserved.
//

#import "MainViewController.h"
#import "TestViewController.h"
#import "ResultViewController.h"
#import "ExternalTestViewController.h"
#import "HomeViewController.h"
@import GoogleMobileAds;

@interface MainViewController ()<GADBannerViewDelegate,GADInterstitialDelegate,GADRewardedAdDelegate>
//橫幅廣告
@property(nonatomic, strong) GADBannerView *bannerView;
//插頁式廣告
@property(nonatomic, strong) GADInterstitial *interstitial;
//獎勵廣告
@property(nonatomic, strong) GADRewardedAd *rewardedAd;

@property (weak, nonatomic) IBOutlet UIImageView *senauLogoView;
@property (weak, nonatomic) IBOutlet UIView *marqueeView;

@property (weak, nonatomic) IBOutlet UIButton *autoTestButton;
@property (weak, nonatomic) IBOutlet UIButton *notAutoTestButton;
@property (weak, nonatomic) IBOutlet UIButton *resultButton;
@property (weak, nonatomic) IBOutlet UIButton *externalTestButton;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIGraphicsBeginImageContext(self.view.frame.size);
//    [[UIImage imageNamed:@"背景色.png"] drawInRect:self.view.bounds];
    [[UIImage imageNamed:@"16pic_7700782_bbbbb.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    [_autoTestButton.layer setCornerRadius:30];
    [_notAutoTestButton.layer setCornerRadius:30];
    [_resultButton.layer setCornerRadius:30];
//    [_externalTestButton.layer setCornerRadius:40];
     
    //插頁式廣告
    [self addInterstitial];
    self.interstitial.delegate = self;
    self.interstitial = [self createAndLoadInterstitial];
    //橫幅廣告
    self.bannerView.delegate = self;
    // In this case, we instantiate the banner with desired ad size.
    self.bannerView = [[GADBannerView alloc]
                       initWithAdSize:kGADAdSizeBanner];
    [self addBannerViewToView:self.bannerView];
    //獎勵廣告
//    self.rewardedAd = [self createAndLoadRewardedAd];
    self.rewardedAd = [self createAndLoadRewardedAdForAdUnit:@"ca-app-pub-6126827794655802/6198748123"];

    
//    GADRewardedAd *gameOverRewardedAd = [self createAndLoadRewardedAdForAdUnit:@"ca-app-pub-6126827794655802/1487754606"];
//    self.rewardedAd = gameOverRewardedAd;
//    GADRewardedAd *extraCoinsRewardedAd = [self   createAndLoadRewardedAdForAdUnit:@"ca-app-pub-6126827794655802/4147553447"];
//    self.rewardedAd = extraCoinsRewardedAd;
    
}

- (void)viewWillAppear:(BOOL)animated {
    
//    if (self.rewardedAd.isReady) {
//        [self.rewardedAd presentFromRootViewController:self delegate:self];
//    } else {
//        NSLog(@"Ad wasn't ready");
//    }
    
    if (self.interstitial.isReady) {
        NSLog(@"廣告準備好了");
        [self.interstitial presentFromRootViewController:self];
    } else {
        NSLog(@"廣告尚未準備好");
    }
    
}

- (GADRewardedAd *)createAndLoadRewardedAd {
    GADRewardedAd *rewardedAd = [[GADRewardedAd alloc]
                                 initWithAdUnitID:@"ca-app-pub-6126827794655802/9571270515"];
    GADRequest *request = [GADRequest request];
    request.testDevices = @[@"bd88753ed8f1a6602678ca15958dcabe"];
    [rewardedAd loadRequest:request completionHandler:^(GADRequestError * _Nullable error) {
        if (error) {
            // Handle ad failed to load case.
        } else {
            // Ad successfully loaded.
        }
    }];
    return rewardedAd;
}

- (void)rewardedAdDidDismiss:(GADRewardedAd *)rewardedAd {
    self.rewardedAd = [self createAndLoadRewardedAd];
}

- (GADRewardedAd *)createAndLoadRewardedAdForAdUnit:(NSString *) adUnitId {
    GADRewardedAd *rewardedAd = [[GADRewardedAd alloc] initWithAdUnitID:adUnitId];
    GADRequest *request = [GADRequest request];
    request.testDevices = @[@"bd88753ed8f1a6602678ca15958dcabe"];
    [rewardedAd loadRequest:request completionHandler:^(GADRequestError * _Nullable error) {
        if (error) {
            // Handle ad failed to load case.
        } else {
            // Ad successfully loaded.
        }
    }];
    return rewardedAd;
}

- (void)addInterstitial {
    self.interstitial = [[GADInterstitial alloc]
                         initWithAdUnitID:@"ca-app-pub-6126827794655802/4953139742"]; //正式
//    self.interstitial = [[GADInterstitial alloc]
//                         initWithAdUnitID:@"ca-app-pub-3940256099942544/4411468910"]; //測試
    GADRequest *request = [GADRequest request];
    request.testDevices = @[@"bd88753ed8f1a6602678ca15958dcabe"];
    [self.interstitial loadRequest:request];
   
}

- (GADInterstitial *)createAndLoadInterstitial {
    GADInterstitial *interstitial =
    [[GADInterstitial alloc] initWithAdUnitID:@"ca-app-pub-6126827794655802/4953139742"]; //正式
//    GADInterstitial *interstitial =
//    [[GADInterstitial alloc] initWithAdUnitID:@"ca-app-pub-3940256099942544/4411468910"]; //測試
    interstitial.delegate = self;
    GADRequest *request = [GADRequest request];
    request.testDevices = @[@"bd88753ed8f1a6602678ca15958dcabe"];
    [interstitial loadRequest:request];
    return interstitial;
}

- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
    self.interstitial = [self createAndLoadInterstitial];
}

- (void)addBannerViewToView:(UIView *)bannerView {
    self.bannerView.translatesAutoresizingMaskIntoConstraints = NO;
    self.bannerView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.width*90/414);
    [self.view addSubview:self.bannerView];
    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:self.bannerView
                                                             attribute:NSLayoutAttributeBottom
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.bottomLayoutGuide
                                                             attribute:NSLayoutAttributeTop
                                                            multiplier:1
                                                              constant:0],
                                [NSLayoutConstraint constraintWithItem:self.bannerView
                                                             attribute:NSLayoutAttributeCenterX
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeCenterX
                                                            multiplier:1
                                                              constant:0]
                                ]];
 
    self.bannerView.adUnitID = @"ca-app-pub-6126827794655802/7957298375"; //正式廣告ID
//    self.bannerView.adUnitID = @"ca-app-pub-3940256099942544/2934735716"; //測試廣告ID
    GADRequest *request = [GADRequest request];
    request.testDevices = @[@"bd88753ed8f1a6602678ca15958dcabe"]; // Sample device ID
    self.bannerView.rootViewController = self;
    [self.bannerView loadRequest:request];
    
}

- (IBAction)dismissMainVC:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)toResultVC:(UIButton *)sender {
    //檢測報告
    ResultViewController *resultViewController = [ResultViewController new];
    [self presentViewController:resultViewController animated:YES completion:nil];
  
}

- (IBAction)resultView:(id)sender {
    //檢測報告
//    ResultViewController *resultViewController = [ResultViewController new];
//    [self presentViewController:resultViewController animated:YES completion:nil];
    
}

- (IBAction)externalTest:(UIButton *)sender {
    //外觀檢測
    [self performSegueWithIdentifier:@"presentETVC" sender:nil];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    //NSLog(@"%d", ((UIButton *)sender).tag);
    
    if (((UIButton *)sender).tag == 1) {
        [(TestViewController *)segue.destinationViewController setIsAuto:YES];
        
        if (self.interstitial.isReady) {
            [self.interstitial presentFromRootViewController:self];
        } else {
            NSLog(@"廣告尚未準備好");
        }
    }
    else if (((UIButton *)sender).tag == 2) {
        [(TestViewController *)segue.destinationViewController setIsAuto:NO];
        
        if (self.rewardedAd.isReady) {
            [self.rewardedAd presentFromRootViewController:self delegate:self];
        } else {
            NSLog(@"Ad wasn't ready");
        }
        
//        if (self.interstitial.isReady) {
//            [self.interstitial presentFromRootViewController:self];
//        } else {
//            NSLog(@"廣告尚未準備好");
//        }
        
    }

//    if ( [segue.identifier isEqualToString:@"presentETVC"]){
//        ExternalTestViewController *ETVC= segue.destinationViewController;
//    }
    
    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        dispatch_async(dispatch_get_main_queue(), ^{
//
//        });
//    });
    
}

@end
