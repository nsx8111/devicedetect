//
//  ResultViewController.m
//  deviceDetect
//
//  Created by 楊智偉 on 2018/11/28.
//  Copyright © 2018 楊智偉. All rights reserved.
//

#import "ResultViewController.h"
#import "UIDevice+Model.h"
#import "EEPowerInformation.h"
#import "ExternalTestViewController.h"
@import GoogleMobileAds;

@interface ResultViewController ()<GADBannerViewDelegate,GADInterstitialDelegate,GADRewardedAdDelegate>

@property(nonatomic, strong) GADRewardedAd *rewardedAd;

@end

@implementation ResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //self.tableView.frame = CGRectInset(self.tableView.frame, 0, 20);
    //[self.tableView clipsToBounds];
    //self.tableView.insetsContentViewsToSafeArea = NO;

    self.tableView.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0);
    
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    
//    [self upload];
    
//    self.rewardedAd = [self createAndLoadRewardedAd];

}

- (GADRewardedAd *)createAndLoadRewardedAd {
    GADRewardedAd *rewardedAd = [[GADRewardedAd alloc]
                                 initWithAdUnitID:@"ca-app-pub-6126827794655802/9571270515"];
    GADRequest *request = [GADRequest request];
    request.testDevices = @[@"bd88753ed8f1a6602678ca15958dcabe"];
    [rewardedAd loadRequest:request completionHandler:^(GADRequestError * _Nullable error) {
        if (error) {
            // Handle ad failed to load case.
        } else {
            // Ad successfully loaded.
        }
    }];
    return rewardedAd;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    if (self.rewardedAd.isReady) {
//        [self.rewardedAd presentFromRootViewController:self delegate:self];
//    } else {
//        NSLog(@"Ad wasn't ready");
//    }
    
    self.restultArray = [[NSMutableArray alloc] initWithContentsOfFile:[self filePath]];
    [self.tableView reloadData];
    
}

- (IBAction)close:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [UIView new];
    view.backgroundColor = UIColor.whiteColor;
    UILabel *modleLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 15, 200, 30)];
    modleLabel.text = [NSString stringWithFormat:@"%@ / %@", [[UIDevice currentDevice] type], [[UIDevice currentDevice] model]];
    [view addSubview:modleLabel];
    
//    UILabel *jbLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 55, 200, 30)];
//    jbLabel.text = [NSString stringWithFormat:@"JB: %@", [[UIDevice currentDevice] isJB] ? @"YES":@"NO"];
//    [view addSubview:jbLabel];
    
//    [UIDevice currentDevice].batteryMonitoringEnabled = YES;
//    EEPowerInformation *powerInformation = [[EEPowerInformation alloc] init];
//    UILabel *batteyLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 95, 200, 30)];
//    batteyLabel.text = [NSString stringWithFormat:@"電池健康度: %f", powerInformation.batteryHealth];
//    [view addSubview:batteyLabel];
//
//    NSLog(@"%@", [NSString stringWithFormat:@"\n\nData updated on %@\n"
//                  @"batteryDesignCapacity=%ld\n"
//                  @"batteryCycleCount=%ld\n"
//                  @"batteryMaximumCapacity=%ld\n"
//                  @"batteryHealth=%f\n"
//                  @"batteryRawBatteryLevel=%f\n"
//                  @"voltage=%f\n"
//                  @"isPluggedIn=%d\n"
//                  @"isCharging=%d\n"
//                  @"isFullyCharged=%d\n"
//                  @"batteryTemperature=%f\n"
//                  @"adapterAmperage=%ld\n"
//                  @"adapterWattage=%ld\n"
//                  @"chargerConfiguration=%ld\n"
//                  @"chargingAmperage=%ld\n"
//                  @"dischargeAmperage=%ld\n"
//                  @"devicePowerConsumption=%f\n",
//                  [powerInformation.dataTimestamp descriptionWithLocale: [NSLocale currentLocale]],
//                  powerInformation.batteryDesignCapacity,
//                  powerInformation.batteryCycleCount,
//                  powerInformation.batteryMaximumCapacity,
//                  powerInformation.batteryHealth,
//                  powerInformation.batteryRawLevel,
//                  powerInformation.voltage,
//                  powerInformation.isPluggedIn,
//                  powerInformation.isCharging,
//                  powerInformation.isFullyCharged,
//                  powerInformation.batteryTemperature,
//                  powerInformation.adapterAmperage,
//                  powerInformation.adapterWattage,
//                  powerInformation.chargerConfiguration,
//                  powerInformation.chargingAmperage,
//                  powerInformation.dischargeAmperage,
//                  powerInformation.devicePowerConsumption]);
//    [UIDevice currentDevice].batteryMonitoringEnabled = NO;
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [UIView new];
    view.backgroundColor = UIColor.grayColor; //是否顯示上傳按鈕
    
    UIButton *button2 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width/2, 50)];
    [button2 setTitle:@"返回首頁" forState:UIControlStateNormal];
    [button2 addTarget:self action:@selector(dismissVC) forControlEvents:UIControlEventTouchUpInside];
    button2.backgroundColor = UIColor.blueColor;
    button2.hidden = NO; //是否顯示上傳按鈕
    [view addSubview:button2];
   
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(tableView.frame.size.width/2, 0, tableView.frame.size.width/2, 50)];
    [button setTitle:@"上傳結果" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(upload) forControlEvents:UIControlEventTouchUpInside];
    button.hidden = NO; //是否顯示上傳按鈕
    [view addSubview:button];
    
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat width = tableView.frame.size.width;
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, width, 50)];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, width*2/3-10, 40)];
    [titleLabel setText:self.restultArray[indexPath.row][@"title"]];
    [cell addSubview:titleLabel];
    
    UILabel *resultLabel = [[UILabel alloc] initWithFrame:CGRectMake(width*2/3, 5, width/3-10, 40)];
    NSString *result;
    switch ([self.restultArray[indexPath.row][@"status"] integerValue]) {
        case 1:
            result = @"Pass";
            [resultLabel setTextColor:UIColor.blueColor];
            break;
        case 0:
            result = @"Fail";
            [resultLabel setTextColor:UIColor.redColor];
            break;
        default:
            result = @"";
            break;
    }
    [resultLabel setText:result];
    [resultLabel setTextAlignment:NSTextAlignmentRight];
    [cell addSubview:resultLabel];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.restultArray.count;
}

- (void)upload {
    [self getToken];
    //[self dismissViewControllerAnimated:YES completion:nil];
}

- (void)dismissVC {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSString*) uuid {
    NSString *result = [self loadData:@"UUID"];
    if (!result) {
        CFUUIDRef puuid = CFUUIDCreate( nil );
        CFStringRef UUIDString = CFUUIDCreateString( nil, puuid );
        result = (NSString *)CFBridgingRelease(CFStringCreateCopy( NULL, UUIDString));
        [self saveData:result key:@"UUID"];
    }
    
    return result;
}

-(void)saveData:(id)object key:(NSString *)key {
    [[NSUserDefaults standardUserDefaults]
     setObject:object forKey:key];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(id)loadData:(NSString *)key {
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

-(void)getToken {
    NSError *error;
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration];

    //NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    //NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    NSURL *url = [NSURL URLWithString:@"http://demo6.shunteam.com/mApp/GetToken"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"POST"];
    NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: [self uuid], @"DeviceId",nil];
    NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];
    [request setHTTPBody:postData];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
        NSLog(@"%@", dict);
        
        if([dict[@"result"] boolValue]) {
            //success
            [self sendResult:dict[@"msg"]];
        } else {
            //fail
        }
        
    }];
    
    [postDataTask resume];
}

-(void)sendResult:(NSString *)token {
    
    NSLog(@"sendResult");
    NSError *error;
    
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration];
    
    //NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    //NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    NSURL *url = [NSURL URLWithString:@"http://demo6.shunteam.com/mApp/InspectionData"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"POST"];
    
    NSMutableDictionary *resultDict = [self deviceInfo];
    [resultDict setValue:token forKey:@"Token"];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    
    //Optionally for time zone conversions
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Taipeis"]];
    
    NSString *stringFromDate = [formatter stringFromDate:[NSDate date]];
    
    [resultDict setValue:stringFromDate forKey:@"TestTime"];
    
    for (NSDictionary *testDict in self.restultArray) {
        NSString *key = [testDict valueForKey:@"key"];
        if ([key isEqualToString:@""]) {
            continue;
        }
        
        if ([key isEqualToString:@"CpuTest"]) {
            id result;
            switch ([[testDict valueForKey:@"status"] integerValue]) {
                case 0:
                    result = @NO;
                    break;
                case 1:
                    result = @YES;
                    break;
                default:
                    result = nil;
                    break;
            }
            [resultDict setValue:result forKey:@"BatteryTest"];
            [resultDict setValue:result forKey:@"RamTest"];
        }

        id result;
        switch ([[testDict valueForKey:@"status"] integerValue]) {
            case 0:
                result = @NO;
                break;
            case 1:
                result = @YES;
                break;
            default:
                result = nil;
                break;
        }

        [resultDict setValue:result forKey:key];
    }
    
//    NSLog(@"%@", resultDict);
    //NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: [self uuid], @"DeviceId",nil];
    NSData *postData = [NSJSONSerialization dataWithJSONObject:resultDict options:0 error:&error];
    
//    NSLog(@"post json:%@", [NSString stringWithUTF8String:[postData bytes]]);
//    NSLog(@"post json post json");

    [request setHTTPBody:postData];
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
        NSLog(@"%@", dict); // { msg = Success;  result = 1; }
        NSLog(@"%@", dict[@"msg"]); //Success
//        NSLog(@"boolValue boolValue boolValue");
//        NSLog(@"%@",response);
        
        if([dict[@"result"] boolValue]) {
            //success
        } else {
            //fail
        }
        
//        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"檢測完成" message:nil preferredStyle:UIAlertControllerStyleAlert];
//
//        UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"確定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
//
//            //        [self performSegueWithIdentifier:@"presentExternalTest" sender:nil];
//            [self dismissViewControllerAnimated:YES completion:nil];
//
//        }];
//
//        [alertController addAction:confirm];
//
//        [self presentViewController:alertController animated:YES completion:nil];
        
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    [postDataTask resume];
}

- (NSMutableDictionary *)deviceInfo {
    NSMutableDictionary *dict = [NSMutableDictionary new];
    
    NSProcessInfo *processInfo = [NSProcessInfo processInfo];
    [dict setValue:[@(processInfo.processorCount) stringValue] forKey:@"Cpu"];
    [dict setValue:[NSString stringWithFormat:@"%@ GB",[@(processInfo.physicalMemory/1024/1024/1024) stringValue]] forKey:@"Ram"];
    
    [dict setValue:[NSString stringWithFormat:@"%@ (%@)", [[UIDevice currentDevice] type], [[UIDevice currentDevice] model]] forKey:@"ModelNo"];
    [dict setValue:[[UIDevice currentDevice] platform] forKey:@"ModelName"];
    [dict setValue:[[UIDevice currentDevice] systemVersion] forKey:@"OsVer"];
    //[dict setValue:@([[UIDevice currentDevice] isJB]) forKey:@"RootedTest"];
    return dict;
}

-(NSString *)filePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"detect.plist"];
    return filePath;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


//- (void)rewardedAd:(nonnull GADRewardedAd *)rewardedAd userDidEarnReward:(nonnull GADAdReward *)reward {
//    <#code#>
//}
//
//- (void)encodeWithCoder:(nonnull NSCoder *)aCoder {
//    <#code#>
//}
//
//- (void)traitCollectionDidChange:(nullable UITraitCollection *)previousTraitCollection {
//    <#code#>
//}
//
//- (void)preferredContentSizeDidChangeForChildContentContainer:(nonnull id<UIContentContainer>)container {
//    <#code#>
//}
//
//- (CGSize)sizeForChildContentContainer:(nonnull id<UIContentContainer>)container withParentContainerSize:(CGSize)parentSize {
//    <#code#>
//}
//
//- (void)systemLayoutFittingSizeDidChangeForChildContentContainer:(nonnull id<UIContentContainer>)container {
//    <#code#>
//}
//
//- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(nonnull id<UIViewControllerTransitionCoordinator>)coordinator {
//    <#code#>
//}
//
//- (void)willTransitionToTraitCollection:(nonnull UITraitCollection *)newCollection withTransitionCoordinator:(nonnull id<UIViewControllerTransitionCoordinator>)coordinator {
//    <#code#>
//}
//
//- (void)didUpdateFocusInContext:(nonnull UIFocusUpdateContext *)context withAnimationCoordinator:(nonnull UIFocusAnimationCoordinator *)coordinator {
//    <#code#>
//}
//
//- (void)setNeedsFocusUpdate {
//    <#code#>
//}
//
//- (BOOL)shouldUpdateFocusInContext:(nonnull UIFocusUpdateContext *)context {
//    <#code#>
//}
//
//- (void)updateFocusIfNeeded {
//    <#code#>
//}


@end
