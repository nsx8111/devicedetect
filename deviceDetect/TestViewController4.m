//
//  FirstViewController.m
//  deviceDetect
//
//  Created by 楊智偉 on 2018/10/17.
//  Copyright © 2018年 楊智偉. All rights reserved.

//#import "RightViewController2.swift"
#import "TestViewController.h"
#import "SharkfoodMuteSwitchDetector.h"
#import "TapViewController.h"
#import "PanViewController.h"
#import "ForceViewController.h"
#import "ScreenViewController.h"
#import "Reachability.h"
#import "AVCamCameraViewController.h"
#import "ResultViewController.h"
#import <CoreMotion/CoreMotion.h>
#import <LocalAuthentication/LocalAuthentication.h>
#import <mach/mach.h>
#import <assert.h>
#import <AudioToolbox/AudioToolbox.h>
#import "UIDevice+Model.h"
//#import "IOPowerSources.h"
//#import "IOPSKeys.h"
#import "MLKMenuPopover.h"
#import "ExternalTestViewController.h"

#define MENU_POPOVER_FRAME  CGRectMake(250, 60, 190, 388)

@interface TestViewController ()<MLKMenuPopoverDelegate> {
    int volume[2];
    //float oriValue;
}
//@property (weak, nonatomic) ResultViewController *resultVC;

@property (weak, nonatomic) IBOutlet UIView *collectionBackgroundView;
 
@property(nonatomic,strong) MLKMenuPopover *menuPopover;
@property(nonatomic,strong) NSArray *menuItems;

@property (strong, nonatomic) NSArray *funcArray;
@property (strong, nonatomic) AVAudioPlayer* audioPlayer;
@property (strong, nonatomic) AVAudioRecorder *audioRecorder;
@property (nonatomic,strong) SharkfoodMuteSwitchDetector* detector;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CBPeripheralManager *peripheralManager;
@property (nonatomic, strong) NSMutableArray *characteristicsArray;
@property (nonatomic, strong) UIAlertController *alertController;
@property (nonatomic, strong) CMMotionManager *mm;
@property (nonatomic) NSInteger currectTest;
@property (nonatomic) float oriValue;
//@property (nonatomic, strong) NSObject *oriObject;
@end

typedef enum : NSUInteger {
    None = -1,
    Pass = 1,
    Fail = 0
}  Status;

@implementation TestViewController

#pragma mark -
#pragma mark UIViewController Life Cycle


//- (void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
//    [super applyLayoutAttributes:layoutAttributes];
//    //设置背景颜色
//    ECCollectionViewLayoutAttributes *ecLayoutAttributes = (ECCollectionViewLayoutAttributes*)layoutAttributes;
//    self.backgroundColor = ecLayoutAttributes.color;
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    //self.isAuto = YES;
    
//    self.menuItems = [NSArray arrayWithObjects:@"系統資訊",@"檢測報告",@"清除結果",@"關於",nil];
    self.menuItems = [NSArray arrayWithObjects:@"檢測報告",nil];
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"背景色.png"] drawInRect:self.view.bounds];
//    [[UIImage imageNamed:@"16pic_7700782_b.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    self.collectionBackgroundView.backgroundColor = [UIColor colorWithPatternImage:image];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *currentVer = [defaults stringForKey:@"version"];
    NSString *ver = @"1.0.6";
    if ([ver isEqualToString:currentVer]) {
        self.funcArray = [[NSMutableArray alloc] initWithContentsOfFile:[self filePath]];
        
    } else {
        [defaults setObject:ver forKey:@"version"];
        [defaults synchronize];
    }
    
    if(self.funcArray == nil)
    {
        self.funcArray = [self reloadFuncArray];
        [self.collectionView reloadData];
        //        NSLog(@"%@",_funcArray);
        //        NSLog(@"funcArray2");
    }
    
    [self.closeButton setHidden:self.isAuto];
    [self resetData:nil];
    self.currectTest = -2;
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.isAuto && self.currectTest == -2) {
        [self autoTest:nil];
    } else if (self.currectTest == -1) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (NSMutableArray *) reloadFuncArray {
    
    //    NSArray *titleArray = @[@"CPU/電池/記憶體"];
    
    NSArray *titleArray = @[@"CPU/電池/記憶體",@"聽筒",@"喇叭",@"麥克風",@"耳機孔",@"充電",@"前鏡頭",@"後鏡頭",@"後雙鏡頭",@"閃光燈",@"HOME鍵",@"休眠鍵",@"指紋辨識",@"臉部辨識",@"靜音/震動切換",@"音量鍵",@"行動網路",@"wifi網路",@"藍牙",@"震動",@"距離感測器",@"GPS",@"電子羅盤",@"方向",@"陀螺儀",@"JB",@"亮暗點測試",@"多點觸碰",@"區塊觸碰",@"3D Touch"];
    
    NSArray *imageArray = @[@"processor.png", @"speaker_phone.png", @"vox_player.png", @"microphone.png", @"headphones.png", @"usb_off.png", @"integrated_webcam.png", @"compact_camera.png", @"compact_camera.png", @"flash_on.png", @"home_button.png", @"shutdown.png", @"touch_id.png", @"face_id.png", @"no_audio.png", @"room_sound.png", @"medium_connection.png", @"wifi_direct_logo.png", @"bluetooth.png", @"vibrate.png", @"", @"gps_device.png", @"compass.png", @"rotation.png", @"gyroscope.png", @"", @"screen.png",  @"circled_menu.png", @"squared_menu.png", @"3d_object.png"];
    
    NSArray *keyArray = @[@"CpuTest", @"EarpieceTest", @"SpeakerTest", @"MicTest", @"HeadphoneHoleTest", @"ChargingHoleTest", @"FrontCameraTest", @"BackCameraTest", @"BackTwinCameraTest", @"FlashLightTest", @"HomeButtonTest", @"PowerButtonTest", @"FingerIdTest", @"FaceIdTest", @"MuteVibrationSwitchTest", @"VolumeButtonTest", @"CellularDataTest", @"WifiTest", @"BlueToothTest", @"VibrationTest", @"RangingSensorTest", @"GpsTest", @"", @"", @"GyroscopeTest", @"RootedTest", @"",  @"MultiTouchTest", @"AreaTouch", @"ThreeDTouchTest"];
    
    NSMutableArray *funcMutableArray = [NSMutableArray new];
    for (int i = 0; i < titleArray.count; ++i) {
        if ([titleArray[i] isEqualToString:@""]) {
            continue;
        }
        [funcMutableArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:titleArray[i], @"title", imageArray[i], @"image", @(None), @"status", keyArray[i], @"key", nil]];
    }
    //    NSLog(@"%@",self.funcMutableArray);
    return funcMutableArray;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
//    if ( [segue.identifier isEqualToString:@"presentResult"]){
//        ResultViewController *RVC= segue.destinationViewController;
//    }
    
//    if ( [segue.identifier isEqualToString:@"presentExternalTest"]){
//        ExternalTestViewController *ETVC= segue.destinationViewController;
//    }

}

#pragma mark -
#pragma mark MLKMenuPopoverDelegate

- (void)menuPopover:(MLKMenuPopover *)menuPopover didSelectMenuItemAtIndex:(NSInteger)selectedIndex
{
    [self.menuPopover dismissMenuPopover];
    NSString *title = [NSString stringWithFormat:@"%@ selected.",[self.menuItems objectAtIndex:selectedIndex]];
    
    if (selectedIndex == 0){
        ResultViewController *resultVC = [ResultViewController new];
        [self presentViewController:resultVC animated:YES completion:nil];
    }
    
    //    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //    [alertView show];
    
}

#pragma mark -
#pragma mark Actions

- (IBAction)rightMenu:(UIButton *)sender {
    
    [self.menuPopover dismissMenuPopover];
    self.menuPopover = [[MLKMenuPopover alloc] initWithFrame:MENU_POPOVER_FRAME menuItems:self.menuItems];
    self.menuPopover.menuPopoverDelegate = self;
    [self.menuPopover showInView:self.view];
}

- (IBAction)resetData:(id)sender
{
    //重置(已隱藏)
    for (NSMutableDictionary *dict in self.funcArray) {
        dict[@"status"] = @(None);
    }
    [self.collectionView reloadData];
    
}

- (IBAction)close:(id)sender
{
    //返回鍵
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)autoTest:(id)sender {
    //一鍵測試(已隱藏)
    [self resetData:nil];
    self.isAuto = YES;
    self.currectTest = -1;
    [self test:0];
}


- (BOOL)canBecomeFirstResponder
{
    return YES;
}

//- (void)viewDidDisappear:(BOOL)animated {
//    [super viewDidDisappear:animated];
//    if (self.isAuto) {
//        [self.funcArray writeToFile:[self filePath] atomically:YES];
//    } else if (self.currectTest == -1) {
//        [self.funcArray writeToFile:[self filePath] atomically:YES];
//    }
//}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableView = nil;
    if (kind == UICollectionElementKindSectionHeader) {
        
        UICollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"Header" forIndexPath:indexPath];
        reusableView = header;
        
    }
    
    //    if (kind == UICollectionElementKindSectionFooter)
    //    {
    //        UICollectionReusableView *footerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView" forIndexPath:indexPath];
    //        footerview.backgroundColor = [UIColor purpleColor];
    //        reusableView = footerview;
    //    }
    return reusableView;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.funcArray.count;
    //    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FunctionCell" forIndexPath:indexPath];
    UIView *backgroundView = [cell viewWithTag:1];
    UILabel *label = [cell viewWithTag:2];
    UIImageView *imageView = [cell viewWithTag:3];
    
    backgroundView.backgroundColor = UIColor.whiteColor;
    imageView.image = [UIImage imageNamed:self.funcArray[indexPath.row][@"image"]];
    switch ([self.funcArray[indexPath.row][@"status"] integerValue]) {
        case None:
            backgroundView.layer.borderColor = [UIColor.grayColor CGColor];
            backgroundView.layer.borderWidth = 1;
            break;
        case Pass:
            backgroundView.layer.borderColor = [UIColor.blueColor CGColor];
            backgroundView.layer.borderWidth = 1.5;
            break;
        case Fail:
            backgroundView.layer.borderColor = [UIColor.redColor CGColor];
            backgroundView.layer.borderWidth = 1.5;
            break;
        default:
            backgroundView.layer.borderColor = [UIColor.blackColor CGColor];
            break;
    }
    
    backgroundView.layer.cornerRadius = 15;
    //backgroundView.layer.borderWidth = 2;
    
    label.text = self.funcArray[indexPath.row][@"title"];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    //    if (self.isAuto) {
    //        [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:YES];
    //
    //    }
    //    [self test:indexPath.row];
    if (!self.isAuto) {
        [self test:indexPath.row];
    }
}

- (void)test:(NSInteger) index {
    if (self.isAuto && index == self.currectTest) {
        return;
    }
    self.currectTest = index;
    self.oriValue = -1;
    //self.funcArray[index][@"status"] = @(Fail);
    NSLog(@"Testing: %d", self.currectTest);
    
    if (self.isAuto && index < self.funcArray.count) {
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:YES];
    }
    
    switch (index) {
        case 0:
        {
            [self showTipAlert:@"CPU/電池/記憶體" message:@"檢測中"];
            NSLog(@"%f", [TestViewController cpuUsage]);
            NSLog(@"%lu", (unsigned long)[TestViewController cpuNumber]);
            NSLog(@"%llul", [TestViewController availableMemory]);
            
            
            if ([TestViewController availableMemory] == NSNotFound) {
                [self failTest];
                NSLog(@"CPU/電池/記憶體 failTest");
                return;
            }
            
            [UIDevice currentDevice].batteryMonitoringEnabled = YES;
            float batteryLevel = [UIDevice currentDevice].batteryLevel;
            [UIDevice currentDevice].batteryMonitoringEnabled = NO;
            if (batteryLevel < 0.0) {
                // -1.0 means battery state is UIDeviceBatteryStateUnknown
                [self failTest];
                return;
            }
            
            NSLog(@"CPU/電池/記憶體");
            
            [self passTest:0];
        }
            break;
        case 1:
        {
            [self playAudioFrom:AVAudioSessionCategoryPlayAndRecord];
            [self showCheckAlert:@"聽筒檢測" message:@"是否有聽到聲音？" yes:^(UIAlertAction * _Nonnull action) {
                [self.audioPlayer stop];
                self.alertController = nil;
                [self passTest:1];
            } no:^(UIAlertAction * _Nonnull action) {
                [self.audioPlayer stop];
                self.alertController = nil;
                [self failTest];
            }];
        }
            break;
        case 2:
        {
            [self playAudioFrom:AVAudioSessionCategoryPlayback];
            [self showCheckAlert:@"喇叭檢測" message:@"是否有聽到聲音？" yes:^(UIAlertAction * _Nonnull action) {
                [self.audioPlayer stop];
                self.alertController = nil;
                [self passTest:2];
            } no:^(UIAlertAction * _Nonnull action) {
                [self.audioPlayer stop];
                self.alertController = nil;
                [self failTest];
            }];
        }
            break;
        case 3:
        {
            [self reocdeAudio];
            [self showDoneAlert:@"麥克風檢測" message:@"請說話來檢測麥克風" done:^(UIAlertAction * _Nonnull action) {
                [self.audioRecorder stop];
                self.alertController = nil;
                [self failTest];
            } ];
        }
            break;
        case 4:
        {
            //self.isAuto = NO;
            [self showDoneAlert:@"耳機孔檢測" message:@"請插入耳機" done:^(UIAlertAction * _Nonnull action) {
                [[NSNotificationCenter defaultCenter] removeObserver:self name:AVAudioSessionRouteChangeNotification object:nil];
                self.alertController = nil;
                [self failTest];
            } ];
            
            [[AVAudioSession sharedInstance] setActive:YES error:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(audioRouteChangeListenerCallback:)   name:AVAudioSessionRouteChangeNotification object:nil];
        }
            break;
        case 5:
        {
            //            [self showDoneAlert:@"充電檢測" message:@"請插充電線" done:^(UIAlertAction * _Nonnull action) {
            //                [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceBatteryStateDidChangeNotification object:nil];
            //                [UIDevice currentDevice].batteryMonitoringEnabled = NO;
            //                self.alertController = nil;
            //                [self failTest];
            //            } ];
            //
            //            [UIDevice currentDevice].batteryMonitoringEnabled = YES;
            //            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pluggedDetected:) name:UIDeviceBatteryStateDidChangeNotification object:nil];
            
            //947的做法
            [UIDevice currentDevice].batteryMonitoringEnabled = YES;
            __weak TestViewController *weakSelf = self;
            __block NSInteger i = self.currectTest;
            NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:.5f repeats:YES block:^(NSTimer *timer){
                switch ([UIDevice currentDevice].batteryState) {
                    case UIDeviceBatteryStateUnknown:
                        [timer invalidate];
                        [weakSelf failTest];
                        break;
                    case UIDeviceBatteryStateUnplugged:
                        break;
                    case UIDeviceBatteryStateCharging:
                        [timer invalidate];
                        [weakSelf passTest:i];
                        break;
                    case UIDeviceBatteryStateFull:
                        [timer invalidate];
                        [weakSelf passTest:i];
                        break;
                    default:
                        break;
                }
            }];
            //[NSTimer scheduledTimerWithTimeInterval:.5f target:self selector:@selector(pluggedDetected:) userInfo:nil repeats:YES];
            
            [self showDoneAlert:@"充電檢測" message:@"請插充電線" done:^(UIAlertAction * _Nonnull action) {
                [timer invalidate];
                [UIDevice currentDevice].batteryMonitoringEnabled = NO;
                self.alertController = nil;
                [self failTest];
            } ];
        }
            break;
        case 6:
        {
            AVCaptureDevice *videoDevice = [AVCaptureDevice defaultDeviceWithDeviceType:AVCaptureDeviceTypeBuiltInWideAngleCamera mediaType:AVMediaTypeVideo position:AVCaptureDevicePositionFront];
            if (videoDevice) {
                AVCamCameraViewController *cameraViewController = [AVCamCameraViewController new];
                cameraViewController.videoDevice = videoDevice;
                __weak TestViewController *weakSelf = self;
                cameraViewController.finished = ^(BOOL success){
                    if (success) {
                        [weakSelf passTest:6];
                    } else {
                        [weakSelf failTest];
                    }
                };
                [self presentViewController:cameraViewController animated:YES completion:nil];
            } else {
                [self failTest];
            }
        }
            break;
        case 7:
        {
            AVCaptureDevice *videoDevice = [AVCaptureDevice defaultDeviceWithDeviceType:AVCaptureDeviceTypeBuiltInWideAngleCamera mediaType:AVMediaTypeVideo position:AVCaptureDevicePositionBack];
            if (videoDevice) {
                AVCamCameraViewController *cameraViewController = [AVCamCameraViewController new];
                cameraViewController.videoDevice = videoDevice;
                __weak TestViewController *weakSelf = self;
                cameraViewController.finished = ^(BOOL success){
                    if (success) {
                        [weakSelf passTest:7];
                    } else {
                        [weakSelf failTest];
                    }
                };
                [self presentViewController:cameraViewController animated:YES completion:nil];
            } else {
                [self failTest];
            }
        }
            break;
        case 8:
        {
            AVCaptureDevice *videoDevice = [AVCaptureDevice defaultDeviceWithDeviceType:AVCaptureDeviceTypeBuiltInTelephotoCamera mediaType:AVMediaTypeVideo position:AVCaptureDevicePositionBack];
            if (videoDevice) {
                AVCamCameraViewController *cameraViewController = [AVCamCameraViewController new];
                cameraViewController.videoDevice = videoDevice;
                __weak TestViewController *weakSelf = self;
                cameraViewController.finished = ^(BOOL success){
                    if (success) {
                        [weakSelf passTest:8];
                    } else {
                        [weakSelf failTest];
                    }
                };
                [self presentViewController:cameraViewController animated:YES completion:nil];
            } else {
                [self failTest];
            }
        }
            break;
        case 9:
        {
            [self turnTorchOn:YES];
            [self showCheckAlert:@"閃光燈檢測" message:@"閃光燈是否有亮？" yes:^(UIAlertAction * _Nonnull action) {
                [self turnTorchOn:NO];
                self.alertController = nil;
                [self passTest:9];
            } no:^(UIAlertAction * _Nonnull action) {
                [self turnTorchOn:NO];
                self.alertController = nil;
                [self failTest];
            }];
        }
        case 10:
        {
            [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(homeKeyTest:) name:UIApplicationDidBecomeActiveNotification object:nil];
            [self showDoneAlert:@"HOME鍵檢測" message:@"請連續點擊HOME鍵兩次" done:^(UIAlertAction * _Nonnull action) {
                [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceBatteryStateDidChangeNotification object:nil];
                [UIDevice currentDevice].batteryMonitoringEnabled = NO;
                self.alertController = nil;
                [self failTest];
            } ];
            //[self showTipAlert:@"HOME鍵檢測" message:@"請連續點擊HOME鍵兩次"];
        }
            break;
        case 11:
        {
            [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(powerKeyTest:) name:UIApplicationUserDidTakeScreenshotNotification object:nil];
            [self showDoneAlert:@"休眠鍵檢測" message:@"請同時按壓休眠鍵及HOME鍵" done:^(UIAlertAction * _Nonnull action) {
                [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceBatteryStateDidChangeNotification object:nil];
                [UIDevice currentDevice].batteryMonitoringEnabled = NO;
                self.alertController = nil;
                [self failTest];
            }];
            //[self showTipAlert:@"休眠鍵檢測" message:@"請同時按壓休眠鍵及HOME鍵"];
        }
            break;
        case 12:
        {
            LAContext *context = [[LAContext alloc] init];
            NSError *error = nil;
            
            if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
                
                if (!error && context.biometryType == LABiometryTypeTouchID) {
                    [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics localizedReason:@"請按壓指紋" reply:^(BOOL success, NSError *error) {
                        if (success || error.code == LAErrorAuthenticationFailed) {
                            [self passTest:12];
                        } else {
                            [self failTest];
                        }
                    }];
                } else {
                    [self failTest];
                }
            }
            
        }
            break;
        case 13:
        {
            LAContext *context = [[LAContext alloc] init];
            NSError *error = nil;
            
            if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
                
                if (!error && context.biometryType == LABiometryTypeFaceID) {
                    [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics localizedReason:@"測試臉部辨識" reply:^(BOOL success, NSError *error) {
                        if (success) {
                            [self passTest:13];
                        } else {
                            [self failTest];
                        }
                    }];
                } else {
                    [self failTest];
                }
            }
            
        }
            break;
        case 14:
        {
            [self showDoneAlert:@"靜音鍵檢測" message:@"請撥動靜音鍵" done:^(UIAlertAction * _Nonnull action) {
                self.detector = nil;
                self.detector.silentNotify = nil;
                self.alertController = nil;
                [self failTest];
            } ];
            
            [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
            [[AVAudioSession sharedInstance] setActive:YES error:nil];
            
            __weak TestViewController *weakSelf = self;
            self.detector = [SharkfoodMuteSwitchDetector shared];
            //self.detector.isMute;
            self.detector.silentNotify = ^(BOOL silent){
                if (weakSelf.oriValue < 0) {
                    if (silent) {
                        weakSelf.oriValue = 1;
                    } else {
                        weakSelf.oriValue = 0;
                    }
                }
                if (silent && weakSelf.oriValue == 0) {
                    [weakSelf passTest:14];
                    weakSelf.detector.silentNotify = nil;
                    weakSelf.detector = nil;
                } else if (!silent && weakSelf.oriValue == 1) {
                    [weakSelf passTest:14];
                    weakSelf.detector.silentNotify = nil;
                    weakSelf.detector = nil;
                }
            };
        }
            break;
        case 15:
        {
            [self showDoneAlert:@"音量鍵檢測" message:@"請調整音量 (+&-都要)" done:^(UIAlertAction * _Nonnull action) {
                //[[NSNotificationCenter defaultCenter] removeObserver:self name:MPMusicPlayerControllerVolumeDidChangeNotification object:nil];
                [[AVAudioSession sharedInstance] removeObserver:self forKeyPath:@"outputVolume" context:nil];
                self.alertController = nil;
                [self failTest];
            }];
            
            volume[0] = 0;
            volume[1] = 0;
            [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
            [[AVAudioSession sharedInstance] setActive:YES error:nil];
            [[AVAudioSession sharedInstance] addObserver:self forKeyPath:@"outputVolume" options:NSKeyValueObservingOptionOld|NSKeyValueObservingOptionNew context:nil];
        }
            break;
        case 16:
        {
            [self showTipAlert:@"行動網路檢測" message:@"檢測中"];
            // Allocate a reachability object
            Reachability* reach = [Reachability reachabilityWithHostname:@"www.google.com"];
            
            // Set the blocks
            reach.reachableBlock = ^(Reachability*reach)
            {
                // keep in mind this is called on a background thread
                // and if you are updating the UI it needs to happen
                // on the main thread, like this:
                [reach stopNotifier];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (reach.isReachableViaWWAN) {
                        [self passTest:16];
                    } else if (reach.isReachableViaWiFi) {
                        [self.alertController dismissViewControllerAnimated:YES completion:^{
                            [self showDoneAlert:@"" message:@"請關閉WiFi再試一次" done:^(UIAlertAction * _Nonnull action) {
                                //                                if (self.isAuto) {
                                //                                    self.currectTest = index - 1;
                                //                                    [self test:index];
                                //                                } else {
                                //                                    [self failTest];
                                //                                }
                                if (self.isAuto) {
                                    self.currectTest = index - 1;
                                }
                                [self test:index];
                                // self.alertController = nil;
                                //[self failTest];
                            }];
                        }];
                    } else {
                        [self failTest];
                    }
                    NSLog(@"REACHABLE! via %@", reach.isReachableViaWWAN ? @"WWAN" : @"WiFi");
                });
            };
            
            reach.unreachableBlock = ^(Reachability*reach)
            {
                NSLog(@"UNREACHABLE!");
                [self failTest];
                [reach stopNotifier];
            };
            
            // Start the notifier, which will cause the reachability object to retain itself!
            [reach startNotifier];
        }
            break;
        case 17:
        {
            [self showTipAlert:@"WiFi檢測" message:@"檢測中"];
            // Allocate a reachability object
            Reachability* reach = [Reachability reachabilityWithHostname:@"www.google.com"];
            //reach.reachableOnWWAN = NO;
            // Set the blocks
            reach.reachableBlock = ^(Reachability*reach)
            {
                // keep in mind this is called on a background thread
                // and if you are updating the UI it needs to happen
                // on the main thread, like this:
                [reach stopNotifier];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (reach.isReachableViaWiFi) {
                        [self passTest:17];
                    } else if (reach.isReachableViaWWAN) {
                        [self.alertController dismissViewControllerAnimated:YES completion:^{
                            [self showDoneAlert:@"" message:@"請開啟WiFi再試一次" done:^(UIAlertAction * _Nonnull action) {
                                //                                if (self.isAuto) {
                                //                                    self.currectTest = index - 1;
                                //                                    [self test:index];
                                //                                } else {
                                //                                    [self failTest];
                                //                                }
                                
                                if (self.isAuto) {
                                    self.currectTest = index - 1;
                                }
                                [self test:index];
                                
                                //self.alertController = nil;
                                //[self failTest];
                            }];
                        }];
                    }else {
                        [self failTest];
                    }
                    NSLog(@"REACHABLE! via %@", reach.isReachableViaWiFi ? @"WiFi" : @"WWAN");
                });
            };
            
            reach.unreachableBlock = ^(Reachability*reach)
            {
                NSLog(@"UNREACHABLE!");
                [self failTest];
                [reach stopNotifier];
            };
            
            // Start the notifier, which will cause the reachability object to retain itself!
            [reach startNotifier];
        }
            break;
        case 18:
        {
            [self showTipAlert:@"藍芽檢測" message:@"檢測中"];
            
            self.characteristicsArray = [NSMutableArray new];
            self.peripheralManager = [[CBPeripheralManager alloc] initWithDelegate:self queue:nil];
        }
            break;
        case 19:
        {
            NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:1 repeats:YES block:^(NSTimer *timer){
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
            }];
            
            [self showCheckAlert:@"震動檢測" message:@"是否有震動？" yes:^(UIAlertAction * _Nonnull action) {
                [self.audioPlayer stop];
                self.alertController = nil;
                [self passTest:19];
                [timer invalidate];
                AudioServicesAddSystemSoundCompletion(kSystemSoundID_Vibrate, NULL, NULL, nil, NULL);
            } no:^(UIAlertAction * _Nonnull action) {
                [self.audioPlayer stop];
                self.alertController = nil;
                [self failTest];
                [timer invalidate];
                AudioServicesAddSystemSoundCompletion(kSystemSoundID_Vibrate, NULL, NULL, nil, NULL);
            }];
        }
            break;
        case 20:
        {
            [self showDoneAlert:@"距離感測器檢測" message:@"請於聽筒上方揮動" done:^(UIAlertAction * _Nonnull action) {
                self.alertController = nil;
                [self failTest];
            } ];
            
            [[UIDevice currentDevice] setProximityMonitoringEnabled:YES];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sensorStateChange:) name:UIDeviceProximityStateDidChangeNotification object:nil];
        }
            break;
        case 21:
        {
            [self showDoneAlert:@"GPS檢測" message:@"檢測中" done:^(UIAlertAction * _Nonnull action) {
                self.alertController = nil;
                [self failTest];
            } ];
            self.locationManager = [CLLocationManager new];
            [self.locationManager requestWhenInUseAuthorization];
            //設定delegate
            self.locationManager.delegate = self;
            //開始更新使用者當前位置
            [self.locationManager startUpdatingLocation];
        }
            break;
        case 22:
        {
            [self showDoneAlert:@"電子羅盤檢測" message:@"請轉動手機方位" done:^(UIAlertAction * _Nonnull action) {
                self.alertController = nil;
                [self failTest];
            } ];
            
            self.locationManager = [CLLocationManager new];
            [self.locationManager requestWhenInUseAuthorization];
            //設定delegate
            self.locationManager.delegate = self;
            //開始更新使用者當前位置
            [self.locationManager startUpdatingHeading];
        }
            break;
        case 23:
        {
            [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
            [[NSNotificationCenter defaultCenter]
             addObserver:self selector:@selector(orientationChanged:)
             name:UIDeviceOrientationDidChangeNotification
             object:[UIDevice currentDevice]];
            
            [self showDoneAlert:@"方向感應器檢測" message:@"請旋轉手機" done:^(UIAlertAction * _Nonnull action) {
                self.alertController = nil;
                [[NSNotificationCenter defaultCenter] removeObserver:UIDeviceOrientationDidChangeNotification];
                [self failTest];
            } ];
        }
            break;
        case 24:
        {
            [self becomeFirstResponder];
            [self showDoneAlert:@"陀螺儀檢測" message:@"請搖動手機" done:^(UIAlertAction * _Nonnull action) {
                //[self resignFirstResponder];
                self.alertController = nil;
                [self failTest];
            }];
            
            self.mm = [CMMotionManager new];
            NSOperationQueue *queue = [NSOperationQueue new];
            [self.mm startDeviceMotionUpdatesToQueue:queue withHandler:^(CMDeviceMotion * __nullable motion, NSError * __nullable error) {
                if (error) {
                    [self.mm stopDeviceMotionUpdates];
                    [self failTest];
                    return;
                }
                
                if (self.oriValue < 0) {
                    self.oriValue = sqrt(pow(motion.attitude.roll, 2) +
                                         pow(motion.attitude.yaw, 2) +
                                         pow(motion.attitude.pitch, 2));
                    return;
                }
                
                float ea = sqrt(pow(motion.attitude.roll, 2) +
                                pow(motion.attitude.yaw, 2) +
                                pow(motion.attitude.pitch, 2));
                if (fabs(ea-self.oriValue) > 0.5) {
                    [self passTest:24];
                    [self.mm stopDeviceMotionUpdates];
                }
                
                //                NSLog(@"%f", sqrt(pow(motion.attitude.roll, 2) +
                //                                  pow(motion.attitude.yaw, 2) +
                //                                  pow(motion.attitude.pitch, 2)));
            }];
            //            [self.mm startGyroUpdatesToQueue:queue withHandler:^(CMGyroData * __nullable gyroData, NSError * __nullable error) {
            //                if (error) {
            //                    [self.mm stopGyroUpdates];
            //                    [self failTest:20];
            //                    return;
            //                }
            //
            //                if (!self.oriObject) {
            //                    self.oriObject = gyroData;
            //                    return;
            //                }
            //
            //                NSLog(@"gyro: %f", gyroData.rotationRate.x);
            //            }];
            
            //tipWithMessage(@"請搖動手機");
        }
            break;
        case 25:
        {
            [self showTipAlert:@"JB" message:@"檢測中"];
            if ([[UIDevice currentDevice] isJB]) {
                [self failTest];
            } else {
                [self passTest:25];
            }
        }
        case 26:
        {
            ScreenViewController *screenViewController = [ScreenViewController new];
            __weak TestViewController *weakSelf = self;
            screenViewController.finished = ^(BOOL success){
                if (success) {
                    [weakSelf passTest:26];
                } else {
                    [weakSelf failTest];
                }
            };
            [self presentViewController:screenViewController animated:YES completion:nil];
        }
            break;
        case 27:
        {
            TapViewController *tapViewController = [TapViewController new];
            tapViewController.tapNum = 2;
            __weak TestViewController *weakSelf = self;
            tapViewController.finished = ^(BOOL success){
                if (success) {
                    [weakSelf passTest:27];
                } else {
                    [weakSelf failTest];
                }
            };
            [self presentViewController:tapViewController animated:YES completion:nil];
        }
            break;
        case 28:
        {
            PanViewController *panViewController = [PanViewController new];
            __weak TestViewController *weakSelf = self;
            panViewController.finished = ^(BOOL success){
                if (success) {
                    [weakSelf passTest:28];
                } else {
                    [weakSelf failTest];
                }
            };
            [self presentViewController:panViewController animated:YES completion:nil];
        }
            break;
        case 29:
        {
            //self.isAuto = YES;
            ForceViewController *forceViewController = [ForceViewController new];
            __weak TestViewController *weakSelf = self;
            forceViewController.finished = ^(BOOL success){
                if (success) {
                    [weakSelf passTest:29];
                } else {
                    [weakSelf failTest];
                }
            };
            [self presentViewController:forceViewController animated:YES completion:nil];
        }
            break;
            
        default:
            if (self.isAuto) {
                [self.funcArray writeToFile:[self filePath] atomically:YES];
            }
            self.currectTest = -1;
            self.isAuto = NO;
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"檢測完成" message:nil preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"確定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                
                ResultViewController *resultViewController = [ResultViewController new];
                [self presentViewController:resultViewController animated:YES completion:nil];
//                [self performSegueWithIdentifier:@"presentExternalTest" sender:nil];
            }];
            [alertController addAction:confirm];
            [self presentViewController:alertController animated:YES completion:nil];
            
            break;
    }
}

- (void)playAudioFrom:(AVAudioSessionCategory) category
{
    [[AVAudioSession sharedInstance] setCategory:category error:nil];
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    NSString* path = [[NSBundle mainBundle] pathForResource:@"opening1" ofType:@"mp3"];
    NSURL* file = [NSURL fileURLWithPath:path];
    // thanks @gebirgsbaerbel
    
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:file error:nil];
    [self.audioPlayer prepareToPlay];
    [self.audioPlayer play];
}

- (void)reocdeAudio {
    //[[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryRecord mode:AVAudioSessionModeMeasurement options:AVAudioSessionCategoryOptionDefaultToSpeaker error:nil];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryRecord error: nil];
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    //[[AVAudioSession sharedInstance] setActive:YES withOptions:AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation error:nil];
    //[[AVAudioSession sharedInstance] setPreferredInput:[[[AVAudioSession sharedInstance] availableInputs] firstObject] error:nil];
    
    /* 不需要保存录音文件 */
    //NSURL *url = [NSURL fileURLWithPath:@"/dev/null"];
    NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:@"audio.ima4"];
    NSURL *url = [[NSURL alloc] initWithString:path];
    
    NSDictionary *settings = [NSDictionary dictionaryWithObjectsAndKeys:
                              @44100.0, AVSampleRateKey,
                              [NSNumber numberWithInt: kAudioFormatAppleIMA4], AVFormatIDKey,
                              @2, AVNumberOfChannelsKey,
                              @16, AVEncoderBitRateKey,
                              [NSNumber numberWithInt: AVAudioQualityMax], AVEncoderAudioQualityKey,
                              nil];
    
    NSError *error;
    self.audioRecorder = [[AVAudioRecorder alloc] initWithURL:url settings:settings error:&error];
    if (self.audioRecorder)
    {
        [self.audioRecorder prepareToRecord];
        self.audioRecorder.meteringEnabled = YES;
        [self.audioRecorder record];
        [NSTimer scheduledTimerWithTimeInterval: 0.3f target: self selector:@selector(levelTimerCallback:) userInfo: nil repeats: YES];
    }
    else
    {
        NSLog(@"%@", [error description]);
        [self failTest];
    }
}

- (void)levelTimerCallback:(NSTimer *)timer {
    [self.audioRecorder updateMeters];
    
    float   level;                // The linear 0.0 .. 1.0 value we need.
    float   minDecibels = -30.0f; // Or use -60dB, which I measured in a silent room.
    float   decibels    = [self.audioRecorder averagePowerForChannel:0];
    
    if (decibels < minDecibels)
    {
        level = 0.0f;
    }
    else if (decibels >= 0.0f)
    {
        level = 1.0f;
    }
    else
    {
        float   root            = 2.0f;
        float   minAmp          = powf(10.0f, 0.05f * minDecibels);
        float   inverseAmpRange = 1.0f / (1.0f - minAmp);
        float   amp             = powf(10.0f, 0.05f * decibels);
        float   adjAmp          = (amp - minAmp) * inverseAmpRange;
        
        level = powf(adjAmp, 1.0f / root);
    }
    
    if (level > 0.4) {
        [timer invalidate];
        [self.audioRecorder stop];
        dispatch_async(dispatch_get_main_queue(), ^{
            //[timer fire];
            [self passTest:3];
        });
    }
    
}


-(void)sensorStateChange:(NSNotification *)notification
{
    if ([[UIDevice currentDevice] proximityState]) {
        [[UIDevice currentDevice] setProximityMonitoringEnabled:NO];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceProximityStateDidChangeNotification object:nil];
        [self passTest:20];
    }
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    // 判断是否为self.myKVO的属性“num”:
    if([keyPath isEqualToString:@"outputVolume"] && object == [AVAudioSession sharedInstance]) {
        //self.funcArray[13][@"status"] = @(Pass);
        if ([[change valueForKey:@"old"] floatValue] > [[change valueForKey:@"new"] floatValue]) {
            volume[0] = 1;
        } else if ([[change valueForKey:@"old"] floatValue] < [[change valueForKey:@"new"] floatValue]) {
            volume[1] = 1;
        }
        
        if (volume[0] && volume[1]) {
            [[AVAudioSession sharedInstance] removeObserver:self forKeyPath:@"outputVolume" context:nil];
            [self passTest:15];
        }
        NSLog(@"\\noldnum:%@ newnum:%@", [change valueForKey:@"old"], [change valueForKey:@"new"]);
    }
}

//-(void)volumeChange:(NSNotification *)notification
//{
//    NSDictionary *interuptionDict = notification.userInfo;
//    NSInteger routeChangeReason = [[interuptionDict valueForKey:MPMusicPlayerControllerVolumeDidChang] integerValue];
//    switch (routeChangeReason) {
//        case AVAudioSessionRouteChangeReasonNewDeviceAvailable:
//            NSLog(@"AVAudioSessionRouteChangeReasonNewDeviceAvailable");
//            tipWithMessage(@"耳机插入");
//            self.funcArray[4][@"status"] = @(Pass);
//            break;
//        case AVAudioSessionRouteChangeReasonOldDeviceUnavailable:
//            NSLog(@"AVAudioSessionRouteChangeReasonOldDeviceUnavailable");
//            tipWithMessage(@"耳机拔出，停止播放操作");
//            [[NSNotificationCenter defaultCenter] removeObserver:self];
//            break;
//        case AVAudioSessionRouteChangeReasonCategoryChange:
//            // called at start - also when other audio wants to play
//            tipWithMessage(@"AVAudioSessionRouteChangeReasonCategoryChange");
//            break;
//    }
//}

- (BOOL)isHeadsetPluggedIn {
    AVAudioSessionRouteDescription* route = [[AVAudioSession sharedInstance] currentRoute];
    for (AVAudioSessionPortDescription* desc in [route outputs]) {
        if ([[desc portType] isEqualToString:AVAudioSessionPortHeadphones])
            return YES;
    }
    return NO;
}

- (void)audioRouteChangeListenerCallback:(NSNotification*)notification
{
    NSDictionary *interuptionDict = notification.userInfo;
    NSInteger routeChangeReason = [[interuptionDict valueForKey:AVAudioSessionRouteChangeReasonKey] integerValue];
    switch (routeChangeReason) {
        case AVAudioSessionRouteChangeReasonNewDeviceAvailable:
            NSLog(@"AVAudioSessionRouteChangeReasonNewDeviceAvailable");
            //tipWithMessage(@"耳机插入");
            [self passTest:4];
            break;
        case AVAudioSessionRouteChangeReasonOldDeviceUnavailable:
            NSLog(@"AVAudioSessionRouteChangeReasonOldDeviceUnavailable");
            //tipWithMessage(@"耳机拔出，停止播放操作");
            [[NSNotificationCenter defaultCenter] removeObserver:self];
            break;
        case AVAudioSessionRouteChangeReasonCategoryChange:
            // called at start - also when other audio wants to play
            //tipWithMessage(@"AVAudioSessionRouteChangeReasonCategoryChange");
            break;
    }
}

//自定提醒窗口
NS_INLINE void tipWithMessage(NSString *message){
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:@"提示" message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
        
        [alerView show];
        
        [alerView performSelector:@selector(dismissWithClickedButtonIndex:animated:) withObject:@[@0, @1] afterDelay:0.9];
        
    });
    
}
//(void(^)(NSString*))block
- (void)showCheckAlert:(NSString *)title message:(NSString *)msg yes:(void (^)(UIAlertAction * _Nonnull))yesAction no:(void (^)(UIAlertAction * _Nonnull))noAction {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *yesAlertAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:yesAction];
    
    [alertController addAction:yesAlertAction];
    
    UIAlertAction *noAlertAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDestructive handler:noAction];
    
    [alertController addAction:noAlertAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)showDoneAlert:(NSString *)title message:(NSString *)msg done:(void (^)(UIAlertAction * _Nonnull))doneAction {
    self.alertController = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *doneAlertAction = [UIAlertAction actionWithTitle:@"DONE" style:UIAlertActionStyleDefault handler:doneAction];
    
    [self.alertController addAction:doneAlertAction];
    
    [self presentViewController:self.alertController animated:YES completion:nil];
}

- (void)showTipAlert:(NSString *)title message:(NSString *)msg {
    self.alertController = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:self.alertController animated:YES completion:nil];
}

- (void)dismissTipAlert {
    [self.alertController dismissViewControllerAnimated:YES completion:nil];
}

//- (void)pluggedDetected:(NSNotification*)notification
//{
//    [self passTest:5];
//    //tipWithMessage(@"插線");
//}

- (void)pluggedDetected:(NSTimer*)timer
{
    [self passTest:5];
    
    NSLog(@"Battery level: %.0f%%", [UIDevice currentDevice].batteryLevel * 100 );
    
    switch ([UIDevice currentDevice].batteryState) {
        case UIDeviceBatteryStateUnknown:
            NSLog(@"unknow device");
            break;
            
        case UIDeviceBatteryStateUnplugged:
            NSLog(@"unplugged");
            
        case UIDeviceBatteryStateCharging:
            NSLog(@"chargeing");
            
        case UIDeviceBatteryStateFull:
            NSLog(@"full");
            
        default:
            break;
    }
    //tipWithMessage(@"插線");
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    //取得當前位置
    CLLocation *currentLocation = [locations lastObject];
    if(currentLocation != nil){
        NSLog(@"%f, %f", currentLocation.coordinate.latitude, currentLocation.coordinate.longitude);
        //停止偵測
        [manager stopUpdatingLocation];
        [self passTest:21];
    }
}

-(void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    if (newHeading.headingAccuracy < 0) {
        NSLog(@"需要校準");
        return;
    }
    
    if (self.oriValue < 0.f) {
        self.oriValue = newHeading.trueHeading;
        return;
    }
    
    if (newHeading != nil) {
        if (fabs(self.oriValue - newHeading.trueHeading) > 60.f) {
            [manager stopUpdatingHeading];
            [self passTest:22];
        }
        
        //NSLog(@"%f", newHeading.trueHeading);
    }
}

- (void) initCamera{
    
    NSLog(@"initCamera");
    dispatch_async(dispatch_get_main_queue(), ^{
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        
        //檢查是否支援此Source Type(相機)
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            NSLog(@"Access Camera Device");
            
            //設定影像來源為相機
            imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
            imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
            imagePicker.delegate = self;
            imagePicker.allowsEditing = YES;
            
            //顯示UIImagePickerController
            [self presentViewController:imagePicker animated:YES completion:nil];
        }
        else {
            //提示使用者，目前設備不支援相機
            NSLog(@"No Camera Device");
        }
        
    });
    
}

//使用者按下確定時
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    //取得剛拍攝的相片(或是由相簿中所選擇的相片)
    UIImage *image=[info objectForKey:UIImagePickerControllerEditedImage];
    
    //設定ImageView的Image物件，例如：
    //yourImageView.image = image;
    
    [picker dismissViewControllerAnimated:YES completion:^{}];
}

//使用者按下取消時
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    //一般情況下沒有什麼特別要做的事情
    
    [picker dismissViewControllerAnimated:YES completion:^{}];
    
}

+ (CGFloat)cpuUsage {
    kern_return_t kr;
    mach_msg_type_number_t count;
    static host_cpu_load_info_data_t previous_info = {0, 0, 0, 0};
    host_cpu_load_info_data_t info;
    
    count = HOST_CPU_LOAD_INFO_COUNT;
    
    kr = host_statistics(mach_host_self(), HOST_CPU_LOAD_INFO, (host_info_t)&info, &count);
    if (kr != KERN_SUCCESS) {
        return -1;
    }
    
    natural_t user   = info.cpu_ticks[CPU_STATE_USER] - previous_info.cpu_ticks[CPU_STATE_USER];
    natural_t nice   = info.cpu_ticks[CPU_STATE_NICE] - previous_info.cpu_ticks[CPU_STATE_NICE];
    natural_t system = info.cpu_ticks[CPU_STATE_SYSTEM] - previous_info.cpu_ticks[CPU_STATE_SYSTEM];
    natural_t idle   = info.cpu_ticks[CPU_STATE_IDLE] - previous_info.cpu_ticks[CPU_STATE_IDLE];
    natural_t total  = user + nice + system + idle;
    previous_info    = info;
    
    return (user + nice + system) * 100.0 / total;
}

+ (NSUInteger)cpuNumber {
    return [NSProcessInfo processInfo].activeProcessorCount;
}

+ (uint64_t)availableMemory {
    vm_statistics64_data_t vmStats;
    mach_msg_type_number_t infoCount = HOST_VM_INFO_COUNT;
    kern_return_t kernReturn = host_statistics(mach_host_self(),
                                               HOST_VM_INFO,
                                               (host_info_t)&vmStats,
                                               &infoCount);
    
    if (kernReturn != KERN_SUCCESS) {
        return NSNotFound;
    }
    
    return vm_page_size * (vmStats.free_count + vmStats.inactive_count);
}

//-(void) motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
//{
//    [self resignFirstResponder];
//    [self passTest:20];
//}

-(void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral
{
    if (peripheral.state == CBPeripheralManagerStateUnknown) {
        [self failTest];
    } else if (peripheral.state == CBPeripheralManagerStatePoweredOff) {
        tipWithMessage(@"請開啟藍牙再試一次");
        [self failTest];
    } else {
        [self passTest:18];
    }
}

-(void)homeKeyTest:(NSNotification*)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [self passTest:10];
}

-(void)powerKeyTest:(NSNotification*)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationUserDidTakeScreenshotNotification object:nil];
    [self passTest:11];
}

-(void)passTest:(NSInteger)j {
    NSInteger i = self.currectTest;
    NSLog(@"pass: %d from: %d", i, j);
    self.funcArray[i][@"status"] = @(Pass);
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.collectionView reloadData];
        if (self.alertController) {
            [self.alertController dismissViewControllerAnimated:YES completion:^{
                if (self.isAuto) {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC/2), dispatch_get_main_queue(), ^{
                        [self test:i+1];
                    });
                    //[self test:i+1];
                }
            }];
            self.alertController = nil;
        } else {
            if (self.isAuto) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC/2), dispatch_get_main_queue(), ^{
                    [self test:i+1];
                });
                //[self test:i+1];
            }
        }
    });
}

-(void)failTest {
    NSInteger i = self.currectTest;
    self.funcArray[i][@"status"] = @(Fail);
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.collectionView reloadData];
        if (self.alertController) {
            [self.alertController dismissViewControllerAnimated:YES completion:^{
                if (self.isAuto) {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC/2), dispatch_get_main_queue(), ^{
                        [self test:i+1];
                    });
                    //[self test:i+1];
                }
            }];
            self.alertController = nil;
        } else {
            if (self.isAuto) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC/2), dispatch_get_main_queue(), ^{
                    [self test:i+1];
                });
                //[self test:i+1];
            }
        }
    });
}

-(NSString *)filePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"detect.plist"];
    return filePath;
}

- (void) turnTorchOn: (bool) on {
    
    // check if flashlight available
    Class captureDeviceClass = NSClassFromString(@"AVCaptureDevice");
    if (captureDeviceClass != nil) {
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        if ([device hasTorch] && [device hasFlash]){
            
            [device lockForConfiguration:nil];
            if (on) {
                [device setTorchMode:AVCaptureTorchModeOn];
                [device setFlashMode:AVCaptureFlashModeOn];
                //torchIsOn = YES; //define as a variable/property if you need to know status
            } else {
                [device setTorchMode:AVCaptureTorchModeOff];
                [device setFlashMode:AVCaptureFlashModeOff];
                //torchIsOn = NO;
            }
            [device unlockForConfiguration];
        }
    }
    
}

- (void)orientationChanged:(NSNotification *)notification {
    UIDevice * device = notification.object;
    switch(device.orientation)
    {
        case UIDeviceOrientationPortrait:
            /* start special animation */
            break;
        case UIDeviceOrientationPortraitUpsideDown:
        case UIDeviceOrientationLandscapeLeft:
        case UIDeviceOrientationLandscapeRight:
            [self passTest:23];
        default:
            break;
    };
}

//- (double)batteryLevel {
//    CFTypeRef blob = IOPSCopyPowerSourcesInfo();
//    CFArrayRef sources = IOPSCopyPowerSourcesList(blob);
//
//    CFDictionaryRef pSource = NULL;
//    const void *psValue;
//
//    int numOfSources = CFArrayGetCount(sources);
//    if (numOfSources == 0) {
//        //powerSourceError = @"No power source found";
//        return -1.0f;
//    }
//
//    for (int i = 0 ; i < numOfSources ; i++)
//    {
//        pSource = IOPSGetPowerSourceDescription(blob, CFArrayGetValueAtIndex(sources, i));
//        if (!pSource) {
//            //powerSourceError = @"Can't get power source description";
//            return -1.0f;
//        }
//        psValue = (CFStringRef)CFDictionaryGetValue(pSource, CFSTR(kIOPSNameKey));
//
//        int curCapacity = 0;
//        int maxCapacity = 0;
//        double percent;
//
//        psValue = CFDictionaryGetValue(pSource, CFSTR(kIOPSCurrentCapacityKey));
//        CFNumberGetValue((CFNumberRef)psValue, kCFNumberSInt32Type, &curCapacity);
//
//        psValue = CFDictionaryGetValue(pSource, CFSTR(kIOPSMaxCapacityKey));
//        CFNumberGetValue((CFNumberRef)psValue, kCFNumberSInt32Type, &maxCapacity);
//
//        NSLog(@"---------");
//        NSLog(@"curCapacity = %d | maxCapacity = %d", curCapacity, maxCapacity);
//        percent = ((double)curCapacity/(double)maxCapacity * 100.0f);
//
//        return percent;
//    }
//    return -1.0f;
//}

@end
