//
//  TapViewController.m
//  deviceDetect
//
//  Created by 楊智偉 on 2018/11/1.
//  Copyright © 2018 楊智偉. All rights reserved.
//

#import "TapViewController.h"

@interface TapViewController ()
@property (nonatomic, strong) NSTimer *timer;
@end

@implementation TapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColor.whiteColor;
    [self.view setMultipleTouchEnabled:YES];
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    [self.view addGestureRecognizer:panGesture];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    tapGesture.numberOfTouchesRequired = 2;
    [self.view addGestureRecognizer:tapGesture];
    
    [self addTapArea:2];
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    __weak TapViewController *weakSelf = self;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:30 repeats:NO block:^(NSTimer *timer){
        [self dismissViewControllerAnimated:YES completion:^{
            if (weakSelf.finished) {
                weakSelf.finished(NO);
            }
        }];
    }];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.timer invalidate];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    if (touches.count != self.tapNum) {
//        return;
//    }
//
//    for (UIView *view in self.view.subviews) {
//        bool cc = NO;
//        for (UITouch *touch in touches) {
//            if(CGRectContainsPoint(view.frame, [touch locationInView:self.view])) {
//                cc = YES;
//                break;
//            }
//
//        }
//
//        if (!cc) {
//            return;
//        }
//
//    }
//
//    if (self.tapNum == 2) {
//        self.tapNum = 4;
//        [self addTapArea:4];
//        return;
//    }
//    //[self.timer fire];
//    [self.timer invalidate];
//
//    __weak TapViewController *weakSelf = self;
//    [self dismissViewControllerAnimated:YES completion:^{
//        if (weakSelf.finished) {
//            weakSelf.finished(YES);
//        }
//    }];
//}

-(void) addTapArea:(NSInteger)tapNum {
    for (UIView *view in self.view.subviews) {
        [view removeFromSuperview];
    }
    CGSize fullSize = self.view.frame.size;
    
    CGPoint tapAreaOrigin = CGPointMake(fullSize.width*(0.3f / 2.f), fullSize.height*(0.3f / 2.f));
    CGSize tapAreaSize = CGSizeMake(self.view.frame.size.width * 0.7f, self.view.frame.size.height * 0.7f);
    
    int i = 0;
    while (i < tapNum) {
        CGFloat length = tapAreaSize.width * 0.3f;
        CGRect rect = CGRectMake(tapAreaOrigin.x + arc4random() % (int)tapAreaSize.width, tapAreaOrigin.y + arc4random() % (int)tapAreaSize.height, length, length);
        
        BOOL intersect = NO;
        for (UIView *view in self.view.subviews) {
            if (CGRectIntersectsRect(view.frame, rect)) {
                intersect = YES;
                break;
            }
        }
        
        if (intersect) {
            continue;
        }
        
        UIView *view = [[UIView alloc] initWithFrame:rect];
        view.backgroundColor = UIColor.grayColor;
        view.userInteractionEnabled = NO;
        view.layer.cornerRadius = length / 2.f;
        
        [self.view addSubview:view];
        ++i;
    }
    
//    for (int i = 0; i < tapNum; ++i) {
//        CGFloat length = tapAreaSize.width * 0.3f;
//        CGRect rect = CGRectMake(tapAreaOrigin.x + arc4random() % (int)tapAreaSize.width, tapAreaOrigin.y + arc4random() % (int)tapAreaSize.height, length, length);
//        
//        BOOL intersect = NO;
//        for (UIView *view in self.view.subviews) {
//            if (CGRectIntersectsRect(view.frame, rect)) {
//                intersect = YES;
//                break;
//            }
//        }
//        
//        UIView *view = [[UIView alloc] initWithFrame:rect];
//        view.backgroundColor = UIColor.blueColor;
//        view.userInteractionEnabled = NO;
//        view.layer.cornerRadius = length / 2.f;
//        
//        [self.view addSubview:view];
//    }
}

-(void)handlePan:(UIPanGestureRecognizer *)sender {
    if (sender.numberOfTouches != self.tapNum) {
        return;
    }
    
    for (UIView *view in self.view.subviews) {
        bool cc = NO;
        for (int i = 0; i < sender.numberOfTouches; ++i) {
            if(CGRectContainsPoint(view.frame, [sender locationOfTouch:i inView:sender.view])) {
                cc = YES;
                break;
            }
        }
//        for (UITouch *touch in touches) {
//            if(CGRectContainsPoint(view.frame, [touch locationInView:self.view])) {
//                cc = YES;
//                break;
//            }
//
//        }
        
        if (!cc) {
            return;
        }
        
    }
    
    if (self.tapNum == 2) {
        self.tapNum = 4;
        [self addTapArea:4];
        return;
    }
    //[self.timer fire];
    [self.timer invalidate];
    
    __weak TapViewController *weakSelf = self;
    [self dismissViewControllerAnimated:YES completion:^{
        if (weakSelf.finished) {
            weakSelf.finished(YES);
        }
    }];
}

-(void)handleTap:(UITapGestureRecognizer *)sender {
    if (sender.numberOfTouches != self.tapNum) {
        return;
    }
    
    for (UIView *view in self.view.subviews) {
        bool cc = NO;
        for (int i = 0; i < sender.numberOfTouches; ++i) {
            if(CGRectContainsPoint(view.frame, [sender locationOfTouch:i inView:sender.view])) {
                cc = YES;
                break;
            }
        }
        //        for (UITouch *touch in touches) {
        //            if(CGRectContainsPoint(view.frame, [touch locationInView:self.view])) {
        //                cc = YES;
        //                break;
        //            }
        //
        //        }
        
        if (!cc) {
            return;
        }
        
    }
    
    if (self.tapNum == 2) {
        self.tapNum = 4;
        sender.numberOfTouchesRequired = self.tapNum;
        [self addTapArea:self.tapNum];
        return;
    }
    //[self.timer fire];
    [self.timer invalidate];
    
    __weak TapViewController *weakSelf = self;
    [self dismissViewControllerAnimated:YES completion:^{
        if (weakSelf.finished) {
            weakSelf.finished(YES);
        }
    }];
}

@end
