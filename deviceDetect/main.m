//
//  main.m
//  deviceDetect
//
//  Created by 楊智偉 on 2018/10/17.
//  Copyright © 2018年 楊智偉. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
