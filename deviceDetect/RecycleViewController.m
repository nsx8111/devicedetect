//
//  RecycleViewController.m
//  deviceDetect
//
//  Created by 洋洋 on 2019/4/16.
//  Copyright © 2019年 楊智偉. All rights reserved.
//

#import "RecycleViewController.h"

@interface RecycleViewController ()

@property (weak, nonatomic) IBOutlet UILabel *recyclingAmount;

@end
@implementation RecycleViewController

//- (IBAction)Back:(UIButton *)sender {
// [self dismissViewControllerAnimated:YES completion:nil];
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"新台幣＄9000元" message:@"是否要回收？" preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *yes = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
    }];
    UIAlertAction *no = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
    }];
    
    [alertController addAction:yes];
    [alertController addAction:no];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
