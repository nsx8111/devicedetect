//
//  UIDevice+Model.h
//  deviceDetect
//
//  Created by 楊智偉 on 2018/12/2.
//  Copyright © 2018 楊智偉. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIDevice (Model)
-(NSString *)model;
-(NSString *)type;
-(NSString *)platform;
-(BOOL)isJB;
@end

NS_ASSUME_NONNULL_END
