//
//  ResultViewController.h
//  deviceDetect
//
//  Created by 楊智偉 on 2018/11/28.
//  Copyright © 2018 楊智偉. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ResultViewController : UITableViewController
@property (copy, nonatomic) NSArray *restultArray;
@end

NS_ASSUME_NONNULL_END
