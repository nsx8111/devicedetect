//
//  PanViewController.m
//  deviceDetect
//
//  Created by 楊智偉 on 2018/11/3.
//  Copyright © 2018 楊智偉. All rights reserved.
//

#import "PanViewController.h"

@interface PanViewController ()
@property (nonatomic, strong) NSTimer *timer;
@end

@implementation PanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColor.whiteColor;
    [self.view setMultipleTouchEnabled:YES];
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    [self.view addGestureRecognizer:panGesture];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self.view addGestureRecognizer:tapGesture];
    
    [self splitScreen];
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    __weak PanViewController *weakSelf = self;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:10 repeats:NO block:^(NSTimer *timer){
        [self dismissViewControllerAnimated:YES completion:^{
            if (weakSelf.finished) {
                weakSelf.finished(NO);
            }
        }];
    }];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.timer invalidate];
}

- (void)splitScreen {
    CGSize fullSize = self.view.frame.size;
    int x = fullSize.width / 50.f;
    int y = fullSize.height / 50.f;;
    CGFloat width = fullSize.width / (float)x;
    CGFloat height = fullSize.height / (float)y;
    for (int i = 0; i < x; ++i) {
        for (int j = 0; j < y; ++j) {
            CGRect rect = CGRectMake(i*width, j*height, width, height);
            UIView *view = [[UIView alloc] initWithFrame:rect];
            view.backgroundColor = UIColor.grayColor;
            view.userInteractionEnabled = NO;
            view.tag = i * y + j + 1;
            view.layer.borderColor = [[UIColor whiteColor] CGColor];
            view.layer.borderWidth = 1.f;
            view.layer.cornerRadius = 5.f;
            
            [self.view addSubview:view];
        }
    }
}

-(void)handlePan:(UIPanGestureRecognizer *)sender {
    CGSize fullSize = self.view.frame.size;
    int x = fullSize.width / 50.f;
    int y = fullSize.height / 50.f;;
    CGFloat width = fullSize.width / (float)x;
    CGFloat height = fullSize.height / (float)y;
    for (int i = 0; i < sender.numberOfTouches; ++i) {
        CGPoint point = [sender locationOfTouch:i inView:sender.view];
        int i = point.x / width;
        int j = point.y / height;
        int tag = i * y + j + 1;
        [[self.view viewWithTag:tag] removeFromSuperview];
    }
    
    __weak PanViewController *weakSelf = self;
    [self.timer invalidate];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:10 repeats:NO block:^(NSTimer *timer){
        [self dismissViewControllerAnimated:YES completion:^{
            if (weakSelf.finished) {
                weakSelf.finished(NO);
            }
        }];
    }];
    
    if(!self.view.subviews.count) {
        [self.timer invalidate];
        __weak PanViewController *weakSelf = self;
        [self dismissViewControllerAnimated:YES completion:^{
            if (weakSelf.finished) {
                weakSelf.finished(YES);
            }
        }];
    }
}

-(void)handleTap:(UIPanGestureRecognizer *)sender {
    CGSize fullSize = self.view.frame.size;
    int x = fullSize.width / 50.f;
    int y = fullSize.height / 50.f;;
    CGFloat width = fullSize.width / (float)x;
    CGFloat height = fullSize.height / (float)y;
    for (int i = 0; i < sender.numberOfTouches; ++i) {
        CGPoint point = [sender locationOfTouch:i inView:sender.view];
        int i = point.x / width;
        int j = point.y / height;
        int tag = i * y + j + 1;
        [[self.view viewWithTag:tag] removeFromSuperview];
    }
    
    __weak PanViewController *weakSelf = self;
    [self.timer invalidate];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:10 repeats:NO block:^(NSTimer *timer){
        [self dismissViewControllerAnimated:YES completion:^{
            if (weakSelf.finished) {
                weakSelf.finished(NO);
            }
        }];
    }];
    
    if(!self.view.subviews.count) {
        [self.timer invalidate];
        __weak PanViewController *weakSelf = self;
        [self dismissViewControllerAnimated:YES completion:^{
            if (weakSelf.finished) {
                weakSelf.finished(YES);
            }
        }];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
