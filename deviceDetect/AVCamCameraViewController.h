/*
See LICENSE.txt for this sample’s licensing information.

Abstract:
View controller for camera interface.
*/

@import UIKit;

@interface AVCamCameraViewController : UIViewController
@property (nonatomic, strong) AVCaptureDevice *videoDevice;
@property (nonatomic, copy) void (^finished)(BOOL success);
@end
