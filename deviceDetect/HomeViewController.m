//
//  HomeViewController.m
//  deviceDetect
//
//  Created by 洋洋 on 2019/4/22.
//  Copyright © 2019年 楊智偉. All rights reserved.
//

#import "HomeViewController.h"
#import "MainViewController.h"
#import "Checkbox.h"

@interface HomeViewController (){
    Checkbox *cbox;
    Checkbox *cbox2;
    Checkbox *cbox3;
    Checkbox *cbox4;
    Checkbox *cbox5;
}

@property (weak, nonatomic) IBOutlet UILabel *label1;

@property (weak, nonatomic) IBOutlet UIButton *toMain;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    cbox = [[Checkbox alloc] initWithFrame:CGRectMake(50, 200, 180, 40)];
    cbox.text = @"  插入SIM卡並連上行動網路";
    cbox.showTextLabel = YES;
    cbox.labelFont = [UIFont systemFontOfSize:22];
    [cbox addTarget:self action:@selector(checkAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cbox];
    
    cbox2 = [[Checkbox alloc] initWithFrame:CGRectMake(50, 270, 180, 40)];
    cbox2.text = @"  WIFI開啟並連上網路";
    cbox2.showTextLabel = YES;
    cbox2.labelFont = [UIFont systemFontOfSize:22];
    [cbox2 addTarget:self action:@selector(checkAction2) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cbox2];
    
    cbox3 = [[Checkbox alloc] initWithFrame:CGRectMake(50, 340, 180, 40)];
    cbox3.text = @"  開啟藍芽";
    cbox3.showTextLabel = YES;
    cbox3.labelFont = [UIFont systemFontOfSize:22];
    [cbox3 addTarget:self action:@selector(checkAction3) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cbox3];
    
    cbox4 = [[Checkbox alloc] initWithFrame:CGRectMake(50, 410, 180, 40)];
    cbox4.text = @"  準備好有線耳機";
    cbox4.showTextLabel = YES;
    cbox4.labelFont = [UIFont systemFontOfSize:22];
    [cbox4 addTarget:self action:@selector(checkAction4) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cbox4];
    
    cbox5 = [[Checkbox alloc] initWithFrame:CGRectMake(50, 480, 180, 40)];
    cbox5.text = @"  準備好充電線及插頭";
    cbox5.showTextLabel = YES;
    cbox5.labelFont = [UIFont systemFontOfSize:22];
    [cbox5 addTarget:self action:@selector(checkAction5) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cbox5];
    
}
//isChecked == 0(未勾選項目)  isChecked == 1(已勾選項目)
- (void) checkAction{
    if (cbox.isChecked == 0) {
        cbox.isChecked = false;
        NSLog(@"%hhd",cbox.isChecked);
    }
    else{
        cbox.isChecked = true;
        NSLog(@"%hhd",cbox.isChecked);
    }
}
- (void) checkAction2{
    if (cbox2.isChecked == 0) {
        cbox2.isChecked = false;
    }
    else{
        cbox2.isChecked = true;
    }
}
- (void) checkAction3{
    if (cbox3.isChecked == 0) {
        cbox3.isChecked = false;
    }
    else{
        cbox3.isChecked = true;
    }
}
- (void) checkAction4{
    if (cbox4.isChecked == 0) {
        cbox4.isChecked = false;
    }
    else{
        cbox4.isChecked = true;
    }
}
- (void) checkAction5{
    if (cbox5.isChecked == 0) {
        cbox5.isChecked = false;
    }
    else{
        cbox5.isChecked = true;
    }
}

- (IBAction)toMainVC:(UIButton *)sender {
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    //isChecked == 1 == true (已勾選項目)
    if (cbox.isChecked == 1 && cbox2.isChecked == 1 && cbox3.isChecked == 1 && cbox4.isChecked == 1 && cbox5.isChecked == 1) {
        if ( [segue.identifier isEqualToString:@"presentMainVC"]){
            MainViewController *VC= segue.destinationViewController;
        }
    }else{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"有項目未完成準備" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *returnToHome = [UIAlertAction actionWithTitle:@"返回" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
        }];
        
        [alertController addAction:returnToHome];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

@end
