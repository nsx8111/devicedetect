//
//  ScreenViewController.m
//  deviceDetect
//
//  Created by 楊智偉 on 2019/1/30.
//  Copyright © 2019 楊智偉. All rights reserved.
//

#import "ScreenViewController.h"

@interface ScreenViewController ()
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic) NSUInteger numOfDot;
@property (nonatomic) NSInteger mode;
@end

@implementation ScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColor.blackColor;
    [self addDot:0];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(fail:)];
    [self.view addGestureRecognizer:tapGesture];
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    __weak ScreenViewController *weakSelf = self;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:10 repeats:NO block:^(NSTimer *timer){
        [self dismissViewControllerAnimated:YES completion:^{
            if (weakSelf.finished) {
                weakSelf.finished(NO);
            }
        }];
    }];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.timer invalidate];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)addDot:(NSInteger) mode {
    self.mode = mode;
    self.numOfDot = arc4random() % 3 + 1;
    NSUInteger tapNum = self.numOfDot;
    
    if (mode) {
        self.view.backgroundColor = UIColor.whiteColor;
    } else {
        self.view.backgroundColor = UIColor.blackColor;
    }
    
    for (UIView *view in self.view.subviews) {
        [view removeFromSuperview];
    }
    CGSize fullSize = self.view.frame.size;
    
    CGPoint tapAreaOrigin = CGPointMake(fullSize.width*(0.3f / 2.f), fullSize.height*(0.3f / 2.f));
    CGSize tapAreaSize = CGSizeMake(self.view.frame.size.width * 0.7f, self.view.frame.size.height * 0.7f);
    
    int i = 0;
    while (i < tapNum) {
        CGFloat length = tapAreaSize.width * 0.3f;
        CGRect rect = CGRectMake(tapAreaOrigin.x + arc4random() % (int)tapAreaSize.width, tapAreaOrigin.y + arc4random() % (int)tapAreaSize.height, length, length);
        
        BOOL intersect = NO;
        for (UIView *view in self.view.subviews) {
            if (CGRectIntersectsRect(view.frame, rect)) {
                intersect = YES;
                break;
            }
        }
        
        if (intersect) {
            continue;
        }
        
        UIView *view = [[UIView alloc] initWithFrame:rect];
        view.backgroundColor = UIColor.clearColor;
        view.userInteractionEnabled = YES;
        view.layer.cornerRadius = length / 2.f;
        view.layer.borderColor = [UIColor.clearColor CGColor];
        view.layer.borderWidth = 1.5;
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(check:)];
        [view addGestureRecognizer:tapGesture];
        
        UIView *dot = [[UIView alloc] initWithFrame:CGRectMake(rect.size.width/2-1, rect.size.height/2-1, 1, 1)];
        
        if (mode) {
            dot.backgroundColor = UIColor.blackColor;
        } else {
            dot.backgroundColor = UIColor.whiteColor;
        }
        
        [view addSubview:dot];
        [self.view addSubview:view];
        ++i;
    }
    
    //    for (int i = 0; i < tapNum; ++i) {
    //        CGFloat length = tapAreaSize.width * 0.3f;
    //        CGRect rect = CGRectMake(tapAreaOrigin.x + arc4random() % (int)tapAreaSize.width, tapAreaOrigin.y + arc4random() % (int)tapAreaSize.height, length, length);
    //
    //        BOOL intersect = NO;
    //        for (UIView *view in self.view.subviews) {
    //            if (CGRectIntersectsRect(view.frame, rect)) {
    //                intersect = YES;
    //                break;
    //            }
    //        }
    //
    //        UIView *view = [[UIView alloc] initWithFrame:rect];
    //        view.backgroundColor = UIColor.blueColor;
    //        view.userInteractionEnabled = NO;
    //        view.layer.cornerRadius = length / 2.f;
    //
    //        [self.view addSubview:view];
    //    }
}

- (void)check:(UITapGestureRecognizer *)tapGesture {
    tapGesture.view.userInteractionEnabled = NO;
    tapGesture.view.layer.borderColor = [UIColor.grayColor CGColor];
    tapGesture.view.subviews.firstObject.hidden = YES;
    --self.numOfDot;
    
    __weak ScreenViewController *weakSelf = self;
    [self.timer invalidate];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:10 repeats:NO block:^(NSTimer *timer){
        [self dismissViewControllerAnimated:YES completion:^{
            if (weakSelf.finished) {
                weakSelf.finished(NO);
            }
        }];
    }];
    
    if (self.numOfDot <= 0) {
        if (self.mode == 0) {
            [self addDot:1];
        } else {
            [self.timer invalidate];
            
            __weak ScreenViewController *weakSelf = self;
            [self dismissViewControllerAnimated:YES completion:^{
                if (weakSelf.finished) {
                    weakSelf.finished(YES);
                }
            }];
        }
        
    }
}

- (void)fail:(UITapGestureRecognizer *)tapGesture {
    __weak ScreenViewController *weakSelf = self;
    [self.timer invalidate];
    [self dismissViewControllerAnimated:YES completion:^{
        if (weakSelf.finished) {
            weakSelf.finished(NO);
        }
    }];
}

@end
