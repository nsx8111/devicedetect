//
//  ForceViewController.m
//  deviceDetect
//
//  Created by 楊智偉 on 2018/11/8.
//  Copyright © 2018年 楊智偉. All rights reserved.
//

#import "ForceViewController.h"

@interface ForceViewController ()
@property (nonatomic, strong) NSTimer *timer;
@end

@implementation ForceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColor.whiteColor;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 240, 240)];
    view.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
    view.backgroundColor = UIColor.clearColor;
    view.layer.cornerRadius = 120;
    view.layer.borderWidth = 2;
    view.layer.borderColor = [UIColor.darkGrayColor CGColor];
    
    [self.view addSubview:view];
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    __weak ForceViewController *weakSelf = self;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:20 repeats:NO block:^(NSTimer *timer){
        [self dismissViewControllerAnimated:YES completion:^{
            if (weakSelf.finished) {
                weakSelf.finished(NO);
            }
        }];
    }];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.timer invalidate];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = touches.anyObject;
    //NSLog(@"%f", touch.force);
    CGFloat radius = touch.force / touch.maximumPossibleForce * 120;
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, radius*2, radius*2)];
    view.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
    view.backgroundColor = UIColor.grayColor;
    view.layer.cornerRadius = radius;
    view.tag = 1;
    
    [self.view addSubview:view];
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = touches.anyObject;
    if (touch.force == touch.maximumPossibleForce) {
        [self.timer invalidate];
        __weak ForceViewController *weakSelf = self;
        [self dismissViewControllerAnimated:YES completion:^{
            if (weakSelf.finished) {
                weakSelf.finished(YES);
            }
        }];
    }
    
    [UIView animateWithDuration:0.3f animations:^{
        UIView *view = [self.view viewWithTag:1];
        CGRect rect = view.frame;
        CGFloat radius = touch.force / touch.maximumPossibleForce * 120;
        rect.size = CGSizeMake(radius*2, radius*2);
        view.frame = rect;
        view.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
        view.layer.cornerRadius = radius;
    }];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UIView *view = [self.view viewWithTag:1];
    [view removeFromSuperview];
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UIView *view = [self.view viewWithTag:1];
    [view removeFromSuperview];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
