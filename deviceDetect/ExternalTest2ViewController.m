//
//  ExternalTest2ViewController.m
//  deviceDetect
//
//  Created by 洋洋 on 2019/4/16.
//  Copyright © 2019年 楊智偉. All rights reserved.
//

#import "ExternalTest2ViewController.h"
#import "ExternalTestViewController.h"
#import "Checkbox.h"

@interface ExternalTest2ViewController (){
    Checkbox *cbox;
    Checkbox *cbox2;
}

@end

@implementation ExternalTest2ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    cbox = [[Checkbox alloc] initWithFrame:CGRectMake(100, 350, 250, 50)];
    cbox.text = @"  YES";
    cbox.showTextLabel = YES;
    cbox.labelFont = [UIFont systemFontOfSize:34];
    [cbox addTarget:self action:@selector(checkAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cbox];
    
    cbox2 = [[Checkbox alloc] initWithFrame:CGRectMake(100, 450, 250, 50)];
    cbox2.text = @"  NO";
    cbox2.showTextLabel = YES;
    cbox2.labelFont = [UIFont systemFontOfSize:34];
    [cbox2 addTarget:self action:@selector(checkAction2) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cbox2];
  
}

- (void) checkAction{
    
    if (cbox.isChecked == 0) {
        cbox2.isChecked = true;
        cbox.isChecked = false;
        cbox2.selected = YES;
        cbox.selected = NO;
        NSLog(@"%hhd",cbox.isChecked);
        NSLog(@"%hhd",cbox2.isChecked);
    }
    else{
        cbox.isChecked = true;
        cbox2.isChecked = false;
        cbox2.selected = NO;
        cbox.selected = YES;
        NSLog(@"%hhd",cbox.isChecked);
        NSLog(@"%hhd",cbox2.isChecked);
    }
    
}

- (void) checkAction2{

    if (cbox2.isChecked == 0) {
        cbox.isChecked = true;
        cbox2.isChecked = false;
        cbox.selected = YES;
        cbox2.selected = NO;
    }
    else{
        cbox2.isChecked = true;
        cbox.isChecked = false;
        cbox.selected = NO;
        cbox2.selected = YES;
    }

}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if (cbox.isChecked == 0 && cbox2.isChecked == 0){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"有項目未完成勾選" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *returnToHome = [UIAlertAction actionWithTitle:@"返回" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
        }];
        
        [alertController addAction:returnToHome];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}

@end
