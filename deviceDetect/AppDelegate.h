//
//  AppDelegate.h
//  deviceDetect
//
//  Created by 楊智偉 on 2018/10/17.
//  Copyright © 2018年 楊智偉. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

