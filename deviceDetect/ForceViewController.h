//
//  ForceViewController.h
//  deviceDetect
//
//  Created by 楊智偉 on 2018/11/8.
//  Copyright © 2018年 楊智偉. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ForceViewController : UIViewController
@property (nonatomic, copy) void (^finished)(BOOL success);
@end

NS_ASSUME_NONNULL_END
