//
//  ExternalTestViewController.m
//  deviceDetect
//
//  Created by 洋洋 on 2019/4/3.
//  Copyright © 2019年 楊智偉. All rights reserved.
//

#import "ExternalTest2ViewController.h"
#import "ExternalTestViewController.h"
#import "MLKMenuPopover.h"
#import "ResultViewController.h"
#import "TestViewController.h"
#import "Checkbox.h"

#define MENU_POPOVER_FRAME  CGRectMake(250, 60, 190, 388)

@interface ExternalTestViewController ()<MLKMenuPopoverDelegate>{
    Checkbox *cbox;
    Checkbox *cbox2;
}

@property (weak, nonatomic) IBOutlet UIView *bar;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property(nonatomic,strong) MLKMenuPopover *menuPopover;
@property(nonatomic,strong) NSArray *menuItems;

@end

@implementation ExternalTestViewController

#pragma mark -
#pragma mark UIViewController Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.title = @"Menu Popover";
    self.menuItems = [NSArray arrayWithObjects:@"系統資訊",@"檢測報告",@"清除結果",@"關於",nil];

    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"背景色.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    
    cbox = [[Checkbox alloc] initWithFrame:CGRectMake(100, 350, 250, 50)];
    cbox.text = @"  YES";
    cbox.showTextLabel = YES;
    cbox.labelFont = [UIFont systemFontOfSize:34];
    [cbox addTarget:self action:@selector(checkAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cbox];
    
    cbox2 = [[Checkbox alloc] initWithFrame:CGRectMake(100, 450, 250, 50)];
    cbox2.text = @"  NO";
    cbox2.showTextLabel = YES;
    cbox2.labelFont = [UIFont systemFontOfSize:34];
    [cbox2 addTarget:self action:@selector(checkAction2) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cbox2];
    
}

- (void) checkAction{
    if (cbox.isChecked == 0) {
        cbox2.isChecked = true;
        cbox.isChecked = false;
        cbox2.selected = YES;
        cbox.selected = NO;
    }
    else{
        cbox.isChecked = true;
        cbox2.isChecked = false;
        cbox2.selected = NO;
        cbox.selected = YES;
    }
}

- (void) checkAction2{
    if (cbox2.isChecked == 0) {
        cbox.isChecked = true;
        cbox2.isChecked = false;
        cbox.selected = YES;
        cbox2.selected = NO;
    }
    else{
        cbox2.isChecked = true;
        cbox.isChecked = false;
        cbox.selected = NO;
        cbox2.selected = YES;
    }
}

- (IBAction)close:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark Actions

- (IBAction)rightMenu:(UIButton *)sender {
    
    // Hide already showing popover
    [self.menuPopover dismissMenuPopover];
    
    self.menuPopover = [[MLKMenuPopover alloc] initWithFrame:MENU_POPOVER_FRAME menuItems:self.menuItems];
 
//    NSLog(@"@@@@@@@@@@@###########");

    self.menuPopover.menuPopoverDelegate = self;
    [self.menuPopover showInView:self.view];
    
}

- (IBAction)next:(UIButton *)sender {
//     [self performSegueWithIdentifier:@"presentExternalTest2" sender:nil];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

    if (cbox.isChecked == 0 && cbox2.isChecked == 0){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"有項目未完成勾選" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *returnToHome = [UIAlertAction actionWithTitle:@"返回" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
        }];
        
        [alertController addAction:returnToHome];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}

#pragma mark -
#pragma mark MLKMenuPopoverDelegate

- (void)menuPopover:(MLKMenuPopover *)menuPopover didSelectMenuItemAtIndex:(NSInteger)selectedIndex
{
    [self.menuPopover dismissMenuPopover];
    
    NSString *title = [NSString stringWithFormat:@"%@ selected.",[self.menuItems objectAtIndex:selectedIndex]];


    if (selectedIndex == 1){
        ResultViewController *resultVC = [ResultViewController new];
        [self presentViewController:resultVC animated:YES completion:nil];
    }
    
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//
//    [alertView show];
    
}

@end
