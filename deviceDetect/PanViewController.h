//
//  PanViewController.h
//  deviceDetect
//
//  Created by 楊智偉 on 2018/11/3.
//  Copyright © 2018 楊智偉. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PanViewController : UIViewController
@property (nonatomic, copy) void (^finished)(BOOL success);
@end

NS_ASSUME_NONNULL_END
