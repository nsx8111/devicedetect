//
//  ExternalTest3ViewController.m
//  deviceDetect
//
//  Created by 洋洋 on 2019/4/16.
//  Copyright © 2019年 楊智偉. All rights reserved.
//

#import "ExternalTest3ViewController.h"
#import "ResultViewController.h"
#import "UIDevice+Model.h"
#import "RecycleViewController.h"
#import "Checkbox.h"

@interface ExternalTest3ViewController (){
//    Checkbox *cbox;
//    Checkbox *cbox2;
}

@property (weak, nonatomic) IBOutlet Checkbox *yes1;
@property (weak, nonatomic) IBOutlet Checkbox *yes2;
@property (weak, nonatomic) IBOutlet Checkbox *yes3;
@property (weak, nonatomic) IBOutlet Checkbox *yes4;
@property (weak, nonatomic) IBOutlet Checkbox *yes5;

@property (weak, nonatomic) IBOutlet Checkbox *no1;
@property (weak, nonatomic) IBOutlet Checkbox *no2;
@property (weak, nonatomic) IBOutlet Checkbox *no3;
@property (weak, nonatomic) IBOutlet Checkbox *no4;
@property (weak, nonatomic) IBOutlet Checkbox *no5;
//暫時隱藏
@property (weak, nonatomic) IBOutlet UIButton *checkboxOutlet;

@end

@implementation ExternalTest3ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_yes1 addTarget:self action:@selector(checkActionYes1) forControlEvents:UIControlEventTouchUpInside];
    [_no1 addTarget:self action:@selector(checkActionNo1) forControlEvents:UIControlEventTouchUpInside];
    [_yes2 addTarget:self action:@selector(checkActionYes2) forControlEvents:UIControlEventTouchUpInside];
    [_no2 addTarget:self action:@selector(checkActionNo2) forControlEvents:UIControlEventTouchUpInside];
    [_yes3 addTarget:self action:@selector(checkActionYes3) forControlEvents:UIControlEventTouchUpInside];
    [_no3 addTarget:self action:@selector(checkActionNo3) forControlEvents:UIControlEventTouchUpInside];
    [_yes4 addTarget:self action:@selector(checkActionYes4) forControlEvents:UIControlEventTouchUpInside];
    [_no4 addTarget:self action:@selector(checkActionNo4) forControlEvents:UIControlEventTouchUpInside];
    [_yes5 addTarget:self action:@selector(checkActionYes5) forControlEvents:UIControlEventTouchUpInside];
    [_no5 addTarget:self action:@selector(checkActionNo5) forControlEvents:UIControlEventTouchUpInside];
    

}

- (void) checkActionYes1{
    if (_yes1.isChecked == 0) {
        _no1.isChecked = true;
        _yes1.isChecked = false;
        _no1.selected = YES;
        _yes1.selected = NO;
    }
    else{
        _yes1.isChecked = true;
        _no1.isChecked = false;
        _no1.selected = NO;
        _yes1.selected = YES;
    }
    NSLog(@"%hhd",_yes1.isChecked);
    NSLog(@"%hhd",_no1.isChecked);
    NSLog(@"_yes1.isChecked");
}
- (void) checkActionNo1{
    if (_no1.isChecked == 0) {
        _yes1.isChecked = true;
        _no1.isChecked = false;
        _yes1.selected = YES;
        _no1.selected = NO;
    }
    else{
        _no1.isChecked = true;
        _yes1.isChecked = false;
        _yes1.selected = NO;
        _no1.selected = YES;
    }
    NSLog(@"%hhd",_yes1.isChecked);
    NSLog(@"%hhd",_no1.isChecked);
    NSLog(@"_no1.isChecked");
}

- (void) checkActionYes2{
    if (_yes2.isChecked == 0) {
        _no2.isChecked = true;
        _yes2.isChecked = false;
        _no2.selected = YES;
        _yes2.selected = NO;
    }
    else{
        _yes2.isChecked = true;
        _no2.isChecked = false;
        _no2.selected = NO;
        _yes2.selected = YES;
    }
}
- (void) checkActionNo2{
    if (_no2.isChecked == 0) {
        _yes2.isChecked = true;
        _no2.isChecked = false;
        _yes2.selected = YES;
        _no2.selected = NO;
    }
    else{
        _no2.isChecked = true;
        _yes2.isChecked = false;
        _yes2.selected = NO;
        _no2.selected = YES;
    }
}

- (void) checkActionYes3{
    if (_yes3.isChecked == 0) {
        _no3.isChecked = true;
        _yes3.isChecked = false;
        _no3.selected = YES;
        _yes3.selected = NO;
    }
    else{
        _yes3.isChecked = true;
        _no3.isChecked = false;
        _no3.selected = NO;
        _yes3.selected = YES;
    }
}
- (void) checkActionNo3{
    if (_no3.isChecked == 0) {
        _yes3.isChecked = true;
        _no3.isChecked = false;
        _yes3.selected = YES;
        _no3.selected = NO;
    }
    else{
        _no3.isChecked = true;
        _yes3.isChecked = false;
        _yes3.selected = NO;
        _no3.selected = YES;
    }
}

- (void) checkActionYes4{
    if (_yes4.isChecked == 0) {
        _no4.isChecked = true;
        _yes4.isChecked = false;
        _no4.selected = YES;
        _yes4.selected = NO;
    }
    else{
        _yes4.isChecked = true;
        _no4.isChecked = false;
        _no4.selected = NO;
        _yes4.selected = YES;
    }
}
- (void) checkActionNo4{
    if (_no4.isChecked == 0) {
        _yes4.isChecked = true;
        _no4.isChecked = false;
        _yes4.selected = YES;
        _no4.selected = NO;
    }
    else{
        _no4.isChecked = true;
        _yes4.isChecked = false;
        _yes4.selected = NO;
        _no4.selected = YES;
    }
}

- (void) checkActionYes5{
    if (_yes5.isChecked == 0) {
        _no5.isChecked = true;
        _yes5.isChecked = false;
        _no5.selected = YES;
        _yes5.selected = NO;
    }
    else{
        _yes5.isChecked = true;
        _no5.isChecked = false;
        _no5.selected = NO;
        _yes5.selected = YES;
    }
}
- (void) checkActionNo5{
    if (_no5.isChecked == 0) {
        _yes5.isChecked = true;
        _no5.isChecked = false;
        _yes5.selected = YES;
        _no5.selected = NO;
    }
    else{
        _no5.isChecked = true;
        _yes5.isChecked = false;
        _yes5.selected = NO;
        _no5.selected = YES;
    }
}

- (IBAction)next:(UIButton *)sender {
    
    if ((_yes1.isChecked == 0 && _no1.isChecked == 0) || (_yes2.isChecked == 0 && _no2.isChecked == 0) || (_yes3.isChecked == 0 && _no3.isChecked == 0) || (_yes4.isChecked == 0 && _no4.isChecked == 0) || (_yes5.isChecked == 0 && _no5.isChecked == 0)) {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"有項目未完成勾選" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *returnToHome = [UIAlertAction actionWithTitle:@"返回" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
        }];
        
        [alertController addAction:returnToHome];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }else{
   
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"外觀檢測完成" message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"確定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
            [self upload];
            [self performSegueWithIdentifier:@"presentRecycle" sender:nil];
        }];
        
        [alertController addAction:confirm];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
}

- (void)viewWillAppear:(BOOL)animated
{
    self.restultArray = [[NSMutableArray alloc] initWithContentsOfFile:[self filePath]];
}

- (void)upload {
    [self getToken];
}

- (void)dismissVC {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSString*) uuid {
    NSString *result = [self loadData:@"UUID"];
    if (!result) {
        CFUUIDRef puuid = CFUUIDCreate( nil );
        CFStringRef UUIDString = CFUUIDCreateString( nil, puuid );
        result = (NSString *)CFBridgingRelease(CFStringCreateCopy( NULL, UUIDString));
        [self saveData:result key:@"UUID"];
    }
    return result;
}

-(void)saveData:(id)object key:(NSString *)key {
    [[NSUserDefaults standardUserDefaults]
     setObject:object forKey:key];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(id)loadData:(NSString *)key {
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

-(void)getToken {
    NSError *error;
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration];
    
    //NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    //NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    NSURL *url = [NSURL URLWithString:@"http://demo6.shunteam.com/mApp/GetToken"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"POST"];
    NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: [self uuid], @"DeviceId",nil];
    NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];
    [request setHTTPBody:postData];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
        NSLog(@"%@", dict);
        
        if([dict[@"result"] boolValue]) {
            //success
            [self sendResult:dict[@"msg"]];
        } else {
            //fail
        }
        
    }];
    
    [postDataTask resume];
}

-(void)sendResult:(NSString *)token {
    
    NSLog(@"sendResult");
    NSError *error;
    
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration];
    
    //NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    //NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    NSURL *url = [NSURL URLWithString:@"http://demo6.shunteam.com/mApp/InspectionData"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"POST"];
    
    NSMutableDictionary *resultDict = [self deviceInfo];
    [resultDict setValue:token forKey:@"Token"];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    
    //Optionally for time zone conversions
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Taipeis"]];
    
    NSString *stringFromDate = [formatter stringFromDate:[NSDate date]];
    
    [resultDict setValue:stringFromDate forKey:@"TestTime"];
    
    for (NSDictionary *testDict in self.restultArray) {
        NSString *key = [testDict valueForKey:@"key"];
        if ([key isEqualToString:@""]) {
            continue;
        }
        
        if ([key isEqualToString:@"CpuTest"]) {
            id result;
            switch ([[testDict valueForKey:@"status"] integerValue]) {
                case 0:
                    result = @NO;
                    break;
                case 1:
                    result = @YES;
                    break;
                default:
                    result = nil;
                    break;
            }
            [resultDict setValue:result forKey:@"BatteryTest"];
            [resultDict setValue:result forKey:@"RamTest"];
        }
        
        id result;
        switch ([[testDict valueForKey:@"status"] integerValue]) {
            case 0:
                result = @NO;
                break;
            case 1:
                result = @YES;
                break;
            default:
                result = nil;
                break;
        }
        
        [resultDict setValue:result forKey:key];
    }
    //NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: [self uuid], @"DeviceId",nil];
    NSData *postData = [NSJSONSerialization dataWithJSONObject:resultDict options:0 error:&error];

    [request setHTTPBody:postData];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
        NSLog(@"%@", dict); // { msg = Success;  result = 1; }
        NSLog(@"%@", dict[@"msg"]); //Success
      
        if([dict[@"result"] boolValue]) {
            //success
        } else {
            //fail
        }
//        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [postDataTask resume];
}

- (NSMutableDictionary *)deviceInfo {
    NSMutableDictionary *dict = [NSMutableDictionary new];
    
    NSProcessInfo *processInfo = [NSProcessInfo processInfo];
    [dict setValue:[@(processInfo.processorCount) stringValue] forKey:@"Cpu"];
    [dict setValue:[NSString stringWithFormat:@"%@ GB",[@(processInfo.physicalMemory/1024/1024/1024) stringValue]] forKey:@"Ram"];
    
    [dict setValue:[NSString stringWithFormat:@"%@ (%@)", [[UIDevice currentDevice] type], [[UIDevice currentDevice] model]] forKey:@"ModelNo"];
    [dict setValue:[[UIDevice currentDevice] platform] forKey:@"ModelName"];
    [dict setValue:[[UIDevice currentDevice] systemVersion] forKey:@"OsVer"];

    return dict;
}

-(NSString *)filePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"detect.plist"];
    return filePath;
}

@end
